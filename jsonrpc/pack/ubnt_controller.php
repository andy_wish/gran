<?php if (!defined("INBOX")) die('not allowed');

class Ubnt_controller extends Controller {

	function __construct(){
	}

/*	function create($params) {
		Core::result(Ubnt::create($params));
	}*/

	function read($params) {
		Core::result(Ubnt::read($params));
	}

/*	function update($params) {
		Core::result(Ubnt::update($params));
	}

	function delete($params) {
		Core::result(Ubnt::delete($params));
	}*/

	function command($params) {
		Core::result(Ubnt::command($params));
	}

	function reload($params) {
		Core::result(Ubnt::reload($params));
	}

	function search($params) {
		Core::result(Ubnt::search($params));
	}

}
