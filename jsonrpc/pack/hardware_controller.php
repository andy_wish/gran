<?php if (!defined("INBOX")) die('not allowed');

class Hardware_controller extends Controller {

	function __construct(){
	}

	function create($params) {
		Core::result(Hardware::create($params));
	}

	function read($params) {
		Core::result(Hardware::read($params));
	}

	function update($params) {
		Core::result(Hardware::update($params));
	}

	function delete($params) {
		Core::result(Hardware::delete($params));
	}

	function reload($params) {
		Core::result(Hardware::reload($params));
	}

	function search($params) {
		Core::result(Hardware::search($params));
	}

}
