<?php if (!defined("INBOX")) die('not allowed');

class Talk_controller extends Controller {

	function __construct(){
	}

	function create($params) {
		Core::result(Talk::create($params));
	}

	function code_create($params) {
		Core::result(Talk::code_create($params));
	}

/*	function read($params) {
		Core::result(Talk::read($params));
	}

	function update($params) {
		Core::result(Talk::update($params));
	}

	function delete($params) {
		Core::result(Talk::delete($params));
	}*/

}
