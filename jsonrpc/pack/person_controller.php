<?php if (!defined("INBOX")) die('not allowed');

class Person_controller extends Controller {

	function __construct(){
	}

	function create($params) {
		Core::result(Person::create($params));
	}

	function from_file($params) {
		Core::result(Person::from_file($params));
	}

	function read($params) {
		Core::result(Person::read($params));
	}

	function update($params) {
		Core::result(Person::update($params));
	}

	function delete($params) {
		Core::result(Person::delete($params));
	}

}
