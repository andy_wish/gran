<?php
//ini_set('memory_limit', '12M');
set_time_limit(1200);
define('INBOX', 1);
define('ROOT_DIR', 'C:/OSPanel/domains/gran');
//define('ROOT_DIR', 'C:/web/www/gran');
define('FILES_DIR', ROOT_DIR.'/files');
require ROOT_DIR.'/config.php';

$hour = date('H');
$minute = date('i');
echo 'Старт '.$hour.':'.$minute.':'.date("s").'<hr />';

//TALK files parsing from almatel
if($hour >= 0 AND $minute >= 0){//можно здесь регулировать время запуска

	require ROOT_DIR.'/lib/db.php';
	DB::init($var["db"]);

	almatel_parsing($var);//Получение списка разговоров с сайта cabinet.almatel.ru

}

echo '<hr />Финиш '.date("H:i:s");
exit(0);

/********************************************************/

/** Almatel parsing **/
function almatel_parsing($var){

	$var["almatel"] = [
		"url" => 'https://cabinet.almatel.ru/',
		"login" => 'USER297143',
		"password" => 'grangran12345',
		"files" => [
			'375053_1.xls',
			'375053_2.xls',
			'375053_3.xls',
			'1486719_1.xls',
			'1486719_2.xls',
			'1486719_3.xls',
		]
	];
	$var["result"] = [
		"error" => false,
		"time_start" => date('Y-m-d H:i:s'),
		"files" => 0,
		"line" => 0,
		"line_insert" => 0
	];

	echo '<strong>'.date("H:i:s").' Авторизация.. </strong>';
	if(almatel_auth($var)) echo 'успешно<br />';
	else die('Auth failed');

	echo '<strong>'.date("H:i:s").' файлов загружается.. </strong>';
	if($files_download = almatel_file_download($var)) echo 'успешно, '.$files_download.' загружено<br />';
	else die('Files download failed');

	echo '<strong>'.date("H:i:s").' заносим в базу данных.. </strong>';
	$var = almatel_file_to_db($var);

	//echo '<pre>';var_dump($var["result"]);echo '</pre>';

	if(!$var["result"]["error"] AND $var["result"]["files"]>0){
		echo 'успешно, '.$var["result"]["files"].' файлов';

		DB::query("UPDATE `basis` SET `content`=?s, `datetime`=?s, `created`=NOW() WHERE `type`='talk_insert_result' LIMIT 1", $var["result"]["files"].';'.$var["result"]["line"].';'.$var["result"]["line_insert"], $var["result"]["time_start"]);

	}else die('Files to db failed. '.$var["result"]["files"].' files');

}
function almatel_auth($var){

	$ch = curl_init();
	$url = $var["almatel"]["url"].'login/';
	curl_setopt($ch, CURLOPT_URL, $url );
	curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);// таймаут4
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// отключаем проверку сертификата
	curl_setopt($ch, CURLOPT_COOKIEJAR, FILES_DIR.'/almatel/cookie.txt'); // сохранять куки в файл
	curl_setopt($ch, CURLOPT_COOKIEFILE, FILES_DIR.'/almatel/cookie.txt');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, [
		'login' => $var["almatel"]["login"],
		'password' => $var["almatel"]["password"]
	]);
	$data = curl_exec($ch);
	curl_close($ch);
	//var_dump($data);
	if(!$data) return false;
	return preg_match('#<a[^>]+href="https://cabinet.almatel.ru/"#Usi', $data);
}
function almatel_file_download($var){
	$i = 0;
	foreach($var["almatel"]["files"] as $file){
		$content = curl($var["almatel"]["url"].'serv-phone/'.$file);
		if(strlen($content) > 200) file_put_contents(FILES_DIR.'/almatel/'.$file, $content);
		else{
			echo 'File save error: '.$file.'<br/>';
			return false;
		}
		$i++;
	}
	return $i;
}
function almatel_file_to_db($var){

	foreach($var["almatel"]["files"] as $file){
		$xml = simplexml_load_file(FILES_DIR.'/almatel/'.$file, "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		$array = json_decode($json, TRUE);
		array_shift($array["Worksheet"]["Table"]["Row"]);

		foreach($array["Worksheet"]["Table"]["Row"] as $line){

			$var["result"]["line"]++;

			//echo '<pre>';var_dump($line["Cell"]);echo '</pre>';

			$talk = [];

			$talk["cost"] = (float)$line["Cell"][5]["Data"][0];
			$talk["cost"] *= 100;
			$talk["cost"] = (int)$talk["cost"];
			if($talk["cost"] == 0) continue;

			$talk["date"] = $line["Cell"][0]["Data"][0];
//echo '<pre>';var_dump($talk["date"]);echo '</pre>';
			if(strlen($talk["date"]) == 19){
				$talk["date"] = date_create_from_format("d.m.Y G:i:s", $talk["date"]);
				date_add($talk["date"], date_interval_create_from_date_string('8 Hours'));
				$talk["date"] = $talk["date"]->format('Y-m-d H:i:s');
			}else $talk["date"] = null;

			$talk["phone_to"] = $line["Cell"][1]["Data"][0];

			$talk["code"] = $line["Cell"][2]["Data"][0];
			$talk["code"] = substr($talk["code"], 10);
			if($talk["code"] == '') $talk["code"] = '0';//Без кода
			for($k=0; $k<10; $k++){
				if(strlen($talk["code"]) >= 10) break;
				else $talk["code"] = '0'.$talk["code"];
			}

			$talk["minute"] = (int)$line["Cell"][3]["Data"][0];
			$talk["direction"] = $line["Cell"][4]["Data"][0];

			DB::query("INSERT IGNORE INTO `talk`(`datetime`, `phone_to`, `code`, `minute`, `direction`, `cost`) VALUES(?s, ?s, ?s, ?i, ?s, ?i)", $talk["date"], $talk["phone_to"], $talk["code"], $talk["minute"], $talk["direction"], $talk["cost"]);
			if(DB::affectedRows()) $var["result"]["line_insert"]++;

			/*echo '<pre>';var_dump($talk);echo '</pre>';
			echo '<pre>';var_dump($var["result"]);echo '</pre>';
			exit;*/
		}
		$var["result"]["files"]++;
		//echo '<pre>';var_dump($var["result"]);echo '</pre>';
	}
	//$var["result"]["time_finish"] = date('d-m-Y H:i:s');
	return $var;
}
function curl( $url ){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url ); // отправляем на
	curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);// таймаут4
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// просто отключаем проверку сертификата
	curl_setopt($ch, CURLOPT_COOKIEJAR, FILES_DIR.'/almatel/cookie.txt'); // сохранять куки в файл
	curl_setopt($ch, CURLOPT_COOKIEFILE,  FILES_DIR.'/almatel/cookie.txt');

	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}