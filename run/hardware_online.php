<?php
//C:\web\php\php.exe C:\web\www\gran\run\hardware_online.php
//c:\OSPanel\modules\php\PHP_7.3-x64\php.exe c:\OSPanel\domains\gran\run\hardware_online.php

ini_set('memory_limit', '12M');
set_time_limit(120);
define('INBOX', 1);
//define('ROOT_DIR', 'C:/OSPanel/domains/gran');
define('ROOT_DIR', 'C:/web/www/gran');

define('FILES_DIR', ROOT_DIR.'/files');
require ROOT_DIR.'/config.php';

echo 'Старт '.date("H:i:s").'<hr />';

require ROOT_DIR.'/app/core/core.php';
require ROOT_DIR.'/lib/db.php';
DB::init($var["db"]);
require ROOT_DIR.'/lib/verify.php';
require ROOT_DIR.'/lib/hardware.php';
require ROOT_DIR.'/lib/ubnt.php';


$result = Ubnt::search([
	"timeout" => 17,
	"save_hardware_results" => 1,
	"save_online_results" => 1,
]);
if(isset($result["error"])) echo $result["error"];


echo '<hr />Финиш '.date("H:i:s");
exit(0);