function Jsonrpc(url, token, lang) {

	this.url =url;
	this.token = token;
	this.lang = lang;

	this.getXmlHttp = function(){var xmlhttp;try {xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}if(!xmlhttp&&typeof XMLHttpRequest!='undefined'){xmlhttp=new XMLHttpRequest();}return xmlhttp;};

	this.request = function(method, params, id=1, async=false, callback=false){

		if(this.token) params["token"]=this.token;
		if(this.lang && typeof(params["lang"])==='undefined') params["lang"]=this.lang;

		var data={"jsonrpc":"2.0", "method":method, "params":params, "id":id};
		var req=this.getXmlHttp();

		req.open('post', this.url, async);

		req.send(JSON.stringify(data));
		if(!async){
			if(req.status==200) return JSON.parse(req.responseText);
		}else if(callback){
			req.onreadystatechange = function () {
				if(req.readyState==4 && req.status==200) return callback(JSON.parse(req.responseText));
			}
		}
		wait.finish(0, 'jsonrpc error');
	};

}