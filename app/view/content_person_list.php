<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
<!--	<div class="col-md-2">
			<h5><i class="far fa-plus-square"></i> добавить</h5>
		</div>-->
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-9">
			<div class="person__search">
				<div class="person__input_count" id="person-count"><?=$var["person"]["total"]?></div>
				<input class="person__input" id="person-search" type="search" name="search" value="" placeholder="поиск" />
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group" enctype="multipart/form-data">
				<input type="file" class="form-control-file" id="person_upload" name="files[]" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
			</div>

			<div class="form-group" id="person_msg_box"></div>

		</div>

	</div>

<?php
//echo '<pre>';var_dump($var["person"]);//exit;
if($var["person"]["total"]>0){
/*	$i = 7;
	$img_h = round(Core::config('img_thumbnail_size_h') / $i);
	$img_w = round(Core::config('img_thumbnail_size_w') / $i);*/
?>
	<div class="row">
		<div class="col-xl-11 col-lg-12 col-md-12">
			<table class="table table-sm table-striped table-bordered" id="person_table">
				<thead>
					<tr>
						<th>ФИО</th>
						<th>Телефон</th>
						<th>Адрес</th>
						<th>Примечание</th>
						<th>Рация</th>
						<th>Место</th>
					</tr>
				</thead>
				<tbody id="person-list">
<?php
	foreach($var["person"]["list"] as $row){
		if($row["closed"]=='') {
			$active_mark='';
		}else{
			$active_mark='text-muted';
		}
?>
					<tr class="<?=$active_mark?>" id="person_<?=$row["id"]?>">
		<!--				<td style="width:2rem" class="text-right">
							<i id="person_<?=$row["id"]?>_id"><?=$row["id"]?></i>
						</td>
						<td style="width:2rem">
							<img src="/files/image/<?=$row["img"]?>" id="person_<?=$row["id"]?>_img" title="/files/image/<?=$row["img"]?>" width="<?=$img_w?>" height="<?=$img_h?>" />
						</td>-->
						<td style="width:18rem">
							<a href="/person/read/<?=$row["id"]?>"><i class="far fa-address-card"></i></a>&nbsp;<strong><?=$row["surname"]?></strong> <?=$row["name"].' '.$row["patronymic"]?>
						</td>
						<td><input type="text" class="form-control form-control-sm py-0" id="person_<?=$row["id"]?>_phone" value="<?=$row["phone"]?>" onKeyUp="person.update_one(<?=$row["id"]?>, 'phone', this.value)" /></td>
						<td><input type="text" class="form-control form-control-sm py-0" id="person_<?=$row["id"]?>_address" value="<?=$row["address"]?>" onKeyUp="person.update_one(<?=$row["id"]?>, 'address', this.value)" /></td>
						<td><input type="text" class="form-control form-control-sm py-0" id="person_<?=$row["id"]?>_description" value="<?=$row["description"]?>" onKeyUp="person.update_one(<?=$row["id"]?>, 'description', this.value)" /></td>
						<td><input type="text" class="form-control form-control-sm py-0" id="person_<?=$row["id"]?>_radio" value="<?=$row["radio"]?>" onKeyUp="person.update_one(<?=$row["id"]?>, 'radio', this.value)" /></td>
						<td style="max-width:9rem;word-wrap:break-word"><?=$row["place_name"]?><br /><?=$row["position_name"]?></td>
					</tr>
<?php
	}
?>
				</tbody>
			</table>
		</div>
	</div>

<?php
}else{
?>
	<div class="row justify-content-center">
		<div class="сol-12 my-2"><p>не найдено</p></div>
	</div>
<?php
}
?>

</div>


<script src="/app/plugin/sheetjs/xlsx.full.min.js" type="text/javascript"></script>
<script src="/app/plugin/sheetjs/jszip.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	document.getElementById('person_upload').addEventListener('change', personFileSelect, false);

	$('#person-search').focus();

	$('#person_list_table').DataTable({
		searching: false,
		ordering:  false,
		dom: '1Bfrtip',
		paging: false,
		info: true,
		//scrollY: 400,
		buttons: ['excel', 'pdf', 'print', 'copy'],
		"oLanguage": {
        "sProcessing":"Подождите...",
        "sLengthMenu":"Показать _MENU_ записей",
        "sZeroRecords":"Записи отсутствуют.",
        "sEmptyTable":"Записи отсутствуют.",
        "sInfo":"Записи с _START_ до _END_ из _TOTAL_ ",
        "sInfoEmpty":"Записи с 0 до 0 из 0 ",
        "sInfoFiltered":"(в _MAX_)",
        "sInfoPostFix":"",
        "sSearch":"Поиск: ",
        "sUrl": "",
        "oPaginate": {
            "sFirst":"Первая",
            "sPrevious":"Предыдущая",
            "sNext":"Следующая",
            "sLast":"Последняя"
        }
    }
	
	});

});


var ExcelToJSON = function(){

	this.parseExcelPerson = function(file){

		var reader = new FileReader();

		reader.onload = function(e){
			var data = e.target.result;
			var workbook = XLSX.read(data, {
				type: 'binary'
			});
			workbook.SheetNames.forEach(function(sheetName){

				var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
				var file = JSON.stringify(XL_row_object);
				file = JSON.parse(file);
				//file.shift();

				$.each(file, function(num_line, line){
//if(line["№"] < 277) return true;
					//console.log(line);
					var params = {
						date_start: line["Дата приема"],
						name: line["Сотрудник"],
						position: line["Должность"],
						place: line["Физическое лицо.Помещение (Физические лица)"],
						birth_date: line["Дата рождения"],
						citizen: line["Страна гражданства"],
						start_date: line["Дата приема"],
						status: line["Состояние"],
						number: line["№"],
					};
					if(typeof(line["Дата увольнения"]) == 'undefined') params.finish_date = '';
					else params.finish_date = line["Дата увольнения"];
					if(typeof(line["Приказ об увольнении номер"]) == 'undefined') params.finish_desc = '';
					else params.finish_desc = line["Приказ об увольнении номер"];
					if(typeof(line["Приказ о приеме"]) == 'undefined') params.start_desc = '';
					else params.start_desc = line["Приказ о приеме"];

					//console.log(params);

					var answer = jsonrpc.request('person.from_file', params);

					if(typeof(answer.result.error) != 'undefined'){
						console.log(answer.result.error);
						$('#person_msg_box').html($('#person_msg_box').html() + '\n' + answer.result.error);
						return true;
					}

					person.total++;
					switch(answer.result.desc){
						case 'exist similar one':
							person.exists++;
						break;
						case 'updated':
							person.updated++;
						break;
						case 'inserted new':
							person.added++;
						break;
					}

					//console.log(answer);
					//console.log(person.total);

					$('#person_msg_box').html('Всего: ' + person.total + '. Изменилось: ' + person.updated + '. Новых: ' +person.added);

				})
				//console.log(JSON.parse(json_object));
				//code.output_box.val(code.output_box.val() + '\n' + json_object);
			})
		};

		reader.onerror = function(ex){
			console.log(ex);
		};

		reader.readAsBinaryString(file);
	};
};

function personFileSelect(evt){
	//var files = evt.target.files; // FileList object
	var xl2json = new ExcelToJSON();
	var person_file = evt.target.files[0];
	$('#person_msg_box').html('');
	$('#person_msg_box').html($('#person_msg_box').html() + ' file reading..\n');
	xl2json.parseExcelPerson(person_file);
}

</script>