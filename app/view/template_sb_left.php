<?php if (!defined("INBOX")) die('separate call');

include 'app/view/block_header.php';?>

<div class="container-fluid">
	<div class="row">


		<div class="col-lg-3 col-md-4 sidebar">
			<?php include 'block_sb_left.php';?>
		</div>

		<main class="container-fluid col-lg-9 col-md-8">

			<div class="row">
				<?php include 'app/view/'.$content_file;?>
			</div>

		</main><!--main-->

	</div><!--row-->
</div>
<?php include 'app/view/block_footer.php';?>