<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">

		<div class="col-lg-3 col-md-4 sidebar">

			<form method="get">
				<div class="form-group">
					<small class="form-text text-muted">Начало периода</small>
					<input type="date" name="date_from" class="form-control form-control-sm" id="date_from" value="<?=substr($var["talk"]["date_from"], 0, 10)?>" />
				</div>
				<div class="form-group">
					<small class="form-text text-muted">Конец периода</small>
					<input type="date" name="date_to" class="form-control form-control-sm" id="date_to" value="<?=substr($var["talk"]["date_to"], 0, 10)?>" />
				</div>
				<div class="form-group">
					<small class="form-text text-muted">Имя</small>
					<select class="form-control form-control-sm" id="name" name="name">
						<option value="all">все</option>
<?php
foreach($var["talk"]["codes"] as $code => $n){
?>
						<option value="<?=$n?>"
							<?=$var["talk"]["name"]==$n? ' selected' : ''?>
						><?=$n?></option>
<?php
}
?>
					</select>
				</div>

				<div class="input-group">
					<button type="submit" class="btn btn-primary">Отчёт</button>
					<div class="input-group-append">
						<div class="input-group-text">Всего&nbsp;<strong id="talk_total_cost_top">0</strong></div>
					</div>
				</div>
			</form>

			<hr class="mt-3 mb-3" />

<?php
if($var["talk"]["insert_result"]["day_ago"] < 1) $notice_color = 'success';
elseif($var["talk"]["insert_result"]["day_ago"] < 2) $notice_color = 'warning';
else $notice_color = 'danger';
?>
			<div class="alert alert-<?=$notice_color?>" role="alert">
				<span title="по <?=$var["talk"]["insert_result"]["time_finish_nice"]?>"><?=$var["talk"]["insert_result"]["time_start_nice"]?></span> загружено<br />
				<b><?=$var["talk"]["insert_result"]["line_insert"]?></b>&nbsp;из&nbsp;<?=$var["talk"]["insert_result"]["line"]?> записей(<?=$var["talk"]["insert_result"]["files"]?>&nbsp;файлов)
			</div>
			<div class="form-group">
				<button class="btn btn-sm btn-warning" id="talk_download" data-href="http://gran/run/boot.php">Загрузить сейчас</button>
				<div id="talk_download_result"></div>
			</div>

			<!--<div class="form-group" enctype="multipart/form-data">
				<label for="upload">Загрузка отчёта</label>
				<input type="file" class="form-control-file" id="talk_upload" name="files[]" multiple accept="application/vnd.ms-excel" />
			</div>

			<div><span id="talk_total_file">0</span> файлов</div>
			<div><span id="talk_total_minute">0</span> минут</div>
			<div><span id="talk_total_cost">0</span> рублей</div>

			<div class="form-group">
				<textarea class="form-control" rows="3" cols="12" id="talk_msg_box"></textarea>
			</div>-->

			<hr class="mt-3 mb-3" />

			<a href="/talk/code">Список кодов</a><br />
			<div class="form-group" enctype="multipart/form-data">
				<label for="code_upload">Загрузка кодов</label>
				<input type="file" class="form-control-file" id="code_upload" name="files[]" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
			</div>

			<div><span id="code_total_code">0</span>&nbsp;кодов</div>
			<div><span id="code_total_name">0</span>&nbsp;имён</div>

			<div class="form-group">
				<textarea class="form-control" rows="3" cols="12" id="code_msg_box"></textarea>
			</div>


		</div>

		<main class="container-fluid col-lg-9 col-md-8">
			<div class="row">

<div class="container">
	<div class="row">
		<div class="col-md-12">

<?php
$current_name = '~temp#-';
$first_line = true;
$all_cost = 0;
$talk_total_cost = 0;
$all_minute = 0;
$talk_total_minute = 0;
$phone_numbers = [];
$phone_numbers_white = [
	'9148516349',
	'9246903240',
	'9644580652',
	'9601478001',
	'9512929368',
	'9148583684',
	'9246914978',
	'9642387793',
	'9148518540',
	'9248540653',
	'9644589523',
	'9642370540',
	'4132623477',
	'9644595327',
	'9642373287'
];

$var["talk_sort"]["list"] = [];
$var["talk_sort"]["talk_total_cost"] = 0;
$var["talk_sort"]["talk_total_minute"] = 0;

//echo '<pre>';var_dump($var["talk"]["list"]);echo '</pre>';exit;

foreach ($var["talk"]["list"] as $row){

	if(isset($var["talk"]["codes"][$row["code"]])) $row["name"] = $var["talk"]["codes"][$row["code"]];
	else $row["name"] = 'ИМЯ НЕ НАЙДЕНО';

	if(!in_array($row["phone_to"], $phone_numbers_white)){
		if(!isset($phone_numbers[$row["phone_to"]])) $phone_numbers[$row["phone_to"]] = [];
		if(!in_array($row["code"], $phone_numbers[$row["phone_to"]])){
			$phone_numbers[$row["phone_to"]][] = $row["code"];
		}
	}

	if($row["name"] != $current_name){
		$current_name = $row["name"];
		$var["talk_sort"]["list"][$current_name]["all_cost"] = 0;
		$var["talk_sort"]["list"][$current_name]["all_minute"] = 0;
	}

	$var["talk_sort"]["list"][$current_name]["list"][] = [
		"date_nice" => $row["date_nice"],
		"direction" => $row["direction"],
		"phone_to" => $row["phone_to"],
		"minute" => $row["minute"],
		"cost" => number_format($row["cost"]/100, 2, '.', ' ')
	];

	$var["talk_sort"]["list"][$current_name]["all_cost"] += $row["cost"];
	$var["talk_sort"]["list"][$current_name]["all_minute"] += $row["minute"];
	$var["talk_sort"]["talk_total_cost"] += $row["cost"];
	$var["talk_sort"]["talk_total_minute"] += $row["minute"];
}
ksort($var["talk_sort"]["list"]);
?>

<table class="table table-striped table-sm row-border" id="talk_list_table">
				<thead>
					<tr class="border-bottom">
						<th>ФИО</th>
						<th>Дата</th>
						<th>Направление</th>
						<th>Номер</th>
						<th>Минуты</th>
						<th>Цена</th>
					</tr>
				</thead>
				<tbody>
<?php

//echo '<pre>';var_dump($var["talk_sort"]["list"]);echo '</pre>';exit;

foreach ($var["talk_sort"]["list"] as $name => $main_row){

	echo '<tr><td class="pt-4"><h5>'.$name.'</h5><td></td></td><td></td><td></td><td></td><td></td></tr>';
	//var_dump($main_row["list"]);
	foreach ($main_row["list"] as $row){
		echo '<tr><td></td>';
			echo '<td class="text-right">'.$row["date_nice"].'</td>';
			echo '<td class="text-muted">'.$row["direction"].'</td>';
			echo '<td>'.$row["phone_to"].'</td>';
			echo '<td class="text-right">'.$row["minute"].'&nbsp;мин.</td>';
			echo '<td class="text-right">'.$row["cost"].'&nbsp;р.</td>';
		echo '</tr>';
	}
		echo '<tr>';
			echo '<td></td><td></td><td></td><td></td>';
			echo '<td class="text-right"><b>'.$main_row["all_minute"].'&nbsp;мин.</b></td>';
			echo '<td class="text-right"><b>'.number_format($main_row["all_cost"]/100, 2, '.', '&nbsp').'&nbsp;р.</b></td>';
		echo '</tr>';
}

		echo '<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		echo '<tr>';
			echo '<td><td></td></td><td></td><td class="text-right pt-5">ВСЕГО:</td>';
			echo '<td class="text-right pt-5"><b>'.$var["talk_sort"]["talk_total_minute"].'&nbsp;мин.</b></td>';
			echo '<td class="text-right pt-5"><b id="talk_total_cost_bottom">'.number_format($var["talk_sort"]["talk_total_cost"]/100, 2, '.', '&nbsp').'&nbsp;р.</b></td>';
		echo '</tr>';
?>

				</tbody>
			</table>

<?php
//echo '-> <pre>';var_dump($var["talk"]["list"]);echo '</pre>';exit;
?>
		</div>
	</div>
</div>
<?php
//echo '<pre>';var_dump($phone_numbers);echo '</pre>';//9098483994
foreach($phone_numbers as $number => $codes){
	//echo $number.': '.count($codes).'<br />';
	if(count($codes) < 2){
		unset($phone_numbers[$number]);
		//echo 'unset<br />';
	}
}
//echo '<pre>';var_dump($phone_numbers);echo '</pre>';
//exit;
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-sm table-striped mt-4">
				<thead>
					<tr>
						<th colspan="2" class="p-2 bg-primary text-white">Номера телефонов с несколькими кодами</th>
					</tr>
				</thead>
				<tbody>
<?php
foreach($phone_numbers as $number => $codes){
	echo '<tr>';
	echo '<td><strong>'.$number.'</strong></td>';
	echo '<td>';
	foreach($codes as $code){

		if(isset($var["talk"]["codes"][$code])) $name = $var["talk"]["codes"][$code];
		else $name = 'ИМЯ НЕ НАЙДЕНО';

		echo $name.'&emsp;';
	}
	echo '</td></tr>';
}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>


			</div>
		</main><!--main-->
	</div><!--row-->
</div>


<!--<script src="/app/plugin/sheetjs/shim.js" type="text/javascript"></script>-->
<script src="/app/plugin/sheetjs/xlsx.full.min.js" type="text/javascript"></script>
<script src="/app/plugin/sheetjs/jszip.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	//document.getElementById('talk_upload').addEventListener('change', talkFileSelect, false);
	document.getElementById('code_upload').addEventListener('change', codeFileSelect, false);

	//$('#date_from').focus();

	$('#talk_total_cost_top').html($('#talk_total_cost_bottom').text());

	$('#talk_list_table').DataTable({
		searching: false,
		ordering:  false,
		dom: '1Bfrtip',
		paging: false,
		info: true,
		//scrollY: 400,
		buttons: ['excel', 'pdf', 'print', 'copy'],
		"oLanguage": {
        "sProcessing":"Подождите...",
        "sLengthMenu":"Показать _MENU_ записей",
        "sZeroRecords":"Записи отсутствуют.",
        "sEmptyTable":"Записи отсутствуют.",
        "sInfo":"Записи с _START_ до _END_ из _TOTAL_ ",
        "sInfoEmpty":"Записи с 0 до 0 из 0 ",
        "sInfoFiltered":"(в _MAX_)",
        "sInfoPostFix":"",
        "sSearch":"Поиск: ",
        "sUrl": "",
        "oPaginate": {
            "sFirst":"Первая",
            "sPrevious":"Предыдущая",
            "sNext":"Следующая",
            "sLast":"Последняя"
        }
    }
	
	});

});

$('#talk_download').on('click', function() {

	$(this).attr('disabled', 'disabled');
	$(this).removeClass('btn-warning').addClass('btn-success');
	$(this).html('загрузка.. подождите 1 мин.');

	const request = new XMLHttpRequest();
	const url = "/run/boot.php";

	request.open("POST", url, true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	request.addEventListener("readystatechange", () => {

		if (request.readyState === 4 && request.status === 200) {
			let obj = request.response;

		console.log(obj);
		$('#talk_download_result').html(obj);
		}
	});
	 
	request.send();
});

var talk = {
	error : {status: false, msg: 'готов к загрузке<br />'},
	output_box : $('#talk_msg_box'),
	total_cost_box : $('#talk_total_cost'),
	total_minute_box : $('#talk_total_minute'),
	total_file_box : $('#talk_total_file'),

	total_cost : 0,
	total_minute : 0,
	total_file : 0,
}
var code = {
	error : {status: false, msg: 'готов к загрузке<br />'},
	output_box : $('#code_msg_box'),
	total_code_box : $('#code_total_code'),
	total_name_box : $('#code_total_name'),

	total_code : 0,
	total_name : 0,
}
var ExcelToJSON = function(){

	this.parseExcelTalk = function(file){
		var reader = new FileReader();

		reader.onload = function(e){
			var data = e.target.result;
			var workbook = XLSX.read(data, {
				type: 'binary'
			});
			workbook.SheetNames.forEach(function(sheetName){

				var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
				var json_object = JSON.stringify(XL_row_object);

				$.each(JSON.parse(json_object), function(num_line, line){
					var params = {};

					if(line["Время звонка"] != ''){
						params.datetime = line["Время звонка"];//2019-10-02 15:20:00
						if(params.datetime.length == 19){
							//console.log(params.datetime);
							params.datetime = moment(params.datetime, "DD.MM.YYYY HH:mm:ss").add(8, 'h');
							params.datetime = moment(params.datetime).format("YYYY-MM-DD HH:mm:ss");
							//console.log(params.datetime);return
						}else {
							console.log(params.datetime + ' DATE WRONG');
							params.datetime = null;
						}
					}else{
						talk.error.status = true;
						talk.error.msg = 'Время звонка не указано.';
					}

					if(line["Телефон вызываемого"] != '') params.phone_to = line["Телефон вызываемого"];
					else{
						talk.error.status = true;
						talk.error.msg = 'Телефон вызываемого не указан.';
					}

					if(line["Телефон звонившего"] != ''){
						params.code = line["Телефон звонившего"];
						if (params.code.length >= 10) {
							params.code = params.code.slice(10);
						}
						if(params.code == '') params.code = '0';//Без кода
						for(var k=0; k<10; k++){
							if(params.code.length >=10) break;
							else params.code = '0' + params.code;
						}
					}else{
						talk.error.status = true;
						talk.error.msg = 'Телефон звонившего не указан.';
					}

					if(line["Количество минут"] != ''){
						params.minute = parseInt(line["Количество минут"]);
						talk.total_minute = talk.total_minute + params.minute;
						$(talk.total_minute_box).html(talk.total_minute);
					}else{
						talk.error.status = true;
						talk.error.msg = 'Количество минут не указано.';
					}

					if(line["Название направления"] != '') params.direction = line["Название направления"];
					else{
						talk.error.status = true;
						talk.error.msg = 'Название направления не указано.';
					}

					if(line["Стоимость звонка, руб"] != ''){
						params.cost = parseInt(line["Стоимость звонка, руб"] * 100);
						if(params.cost == 0) return true;
						talk.total_cost = talk.total_cost + params.cost;
						$(talk.total_cost_box).html(number_format(talk.total_cost / 100, 2, '.', '&nbsp;'));
					}else{
						talk.error.status = true;
						talk.error.msg = 'Стоимость звонка не указана.';
					}

					//console.log(params);
					talk.output_box.val(params.datetime + ': ' + params.code+ '\n' + code.output_box.val());

					if(talk.error.status) {
						talk.output_box.val(talk.output_box.val() + '\n' + talk.error.msg);
						return true;
					}

					var answer = jsonrpc.request('talk.create', params);
					if(typeof(answer.result.error) != 'undefined'){
						console.log(answer.result.error);
						talk.output_box.val(talk.output_box.val() + '\n' + answer.result.error);
						return true;
					}
					console.log(answer);
					return true;
				})
				//console.log(JSON.parse(json_object));
				//talk.output_box.val(talk.output_box.val() + '\n' + json_object);
			})
		};

		reader.onerror = function(ex){
			console.log(ex);
		};

		reader.readAsBinaryString(file);
	};

	this.parseExcelCode = function(file){

		var reader = new FileReader();

		reader.onload = function(e){
			var data = e.target.result;
			var workbook = XLSX.read(data, {
				type: 'binary'
			});
			workbook.SheetNames.forEach(function(sheetName){

				var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
				var file = JSON.stringify(XL_row_object);
				file = JSON.parse(file);
				file.shift();

				$.each(file, function(num_line, line){
					var params = {
						code: false,
						name: ''
					};

					if(typeof(line["__EMPTY"]) != 'undefined'){
						params.code = String(line["__EMPTY"]);
						if(params.code.length < 10){
							for(var k=0; k<10; k++){
								if(params.code.length >=10) break;
								else params.code = '0' + params.code;
							}
						}
						code.total_code++;
						$(code.total_code_box).html(code.total_code);
					}else{
						code.error.status = true;
						code.error.msg = 'Код не найден.';
					}

					if(typeof(line["__EMPTY_1"]) != 'undefined'){
						params.name = String(line["__EMPTY_1"]);
						if(params.name.length > 1){
							$(code.total_name_box).html(code.total_name++);
						}else{
							params.name = '-НЕ ВЫДАН';
						}
					}else params.name = '-НЕ ВЫДАН';

					code.output_box.val(params.code + ': ' + params.name+ '\n' + code.output_box.val());
					//console.log(params);

					if(code.error.status) {
						code.output_box.val(code.output_box.val() + '\n' + code.error.msg);
						return true;
					}

					var answer = jsonrpc.request('talk.code_create', params);
					if(typeof(answer.result.error) != 'undefined'){
						console.log(answer.result.error);
						code.output_box.val(code.output_box.val() + '\n' + answer.result.error);
						return true;
					}
					console.log(answer);
					return true;
				})
				//console.log(JSON.parse(json_object));
				//code.output_box.val(code.output_box.val() + '\n' + json_object);
			})
		};

		reader.onerror = function(ex){
			console.log(ex);
		};

		reader.readAsBinaryString(file);
	};
};
function talkFileSelect(evt){
	//var files = evt.target.files; // FileList object
	var xl2json = new ExcelToJSON();
	talk.output_box.val('');
	$.each(evt.target.files, function(fnum, file){
		var file_num = fnum + 1;
		$(talk.total_file_box).html(file_num);
		talk.output_box.val(talk.output_box.val() + file_num);
		talk.output_box.val(talk.output_box.val() + ' file reading..\n');
		xl2json.parseExcelTalk(file);
	});
}
function codeFileSelect(evt){
	//var files = evt.target.files; // FileList object
	var xl2json = new ExcelToJSON();
	var code_file = evt.target.files[0];
	code.output_box.val('');
	code.output_box.val(code.output_box.val() + ' file reading..\n');
	xl2json.parseExcelCode(code_file);
}

</script>