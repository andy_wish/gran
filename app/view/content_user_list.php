<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
		<div class="col-md-2">
			<h5><i class="far fa-plus-square"></i> добавить</h5>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-10">
			<div class="user__search">
				<div class="user__input_count" id="user-count"><?=$var["user"]["total"]?></div>
			</div>
		</div>
	</div>

<?php
//echo '<pre>';var_dump($var["user"]);//exit;
if($var["user"]["total"]>0){
/*	$i = 7;
	$img_h = round(Core::config('img_thumbnail_size_h') / $i);
	$img_w = round(Core::config('img_thumbnail_size_w') / $i);*/
?>
	<div class="row">
		<div class="col-xl-11 col-lg-12 col-md-12">
			<table class="table table-sm table-striped table-bordered" id="user_table">
				<thead>
					<tr>
						<th style="width:1rem" class="text-right">id</th>
						<th style="width:8rem">name</th>
						<th style="width:6rem">группа</th>
					</tr>
				</thead>
				<tbody id="user-list">
<?php
	foreach($var["user"]["list"] as $row){
		if($row["closed"]=='') {
			$active_mark='';
		}else{
			$active_mark='text-muted';
		}
?>
					<tr class="<?=$active_mark?>" id="user_<?=$row["id"]?>">
						<td>
							<i id="user_<?=$row["id"]?>_id"><?=$row["id"]?></i>
						</td>
						<td>
							<a href="/user/read/<?=$row["id"]?>"><i class="far fa-address-card"></i></a>&nbsp; <?=$row["name"]?>
						</td>
						<td>
							<?=$row["group_name"]?>
						</td>
					</tr>
<?php
	}
?>
				</tbody>
			</table>
		</div>
	</div>

<?php
}else{
?>
	<div class="row justify-content-center">
		<div class="сol-12 my-2"><p>не найдено</p></div>
	</div>
<?php
}
?>

</div>
<script type="text/javascript">
$(document).ready(function(){
	//$('#user-search').focus();
});
</script>