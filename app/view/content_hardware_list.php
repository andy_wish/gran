<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">

		<div class="col-lg-3 col-md-4 sidebar">

			<div class="btn-toolbar mb-3" role="toolbar" aria-label="">
				<div class="btn-group btn-group-sm mr-2" role="group" aria-label="">
					<button type="button" class="btn btn-primary" id="ubnt_btn_search" onClick="ubnt.search()">search</button>
				</div>
				<div class="btn-group btn-group-sm mr-2" role="group" aria-label="">
					<button type="button" class="btn btn-danger" onClick="ubnt.reboot_list()">reboot</button>
				</div>
				<div id="ubnt_infobox"><?=$var["hardware"]["total"]?></div>
			</div>

		</div>


		<main class="container-fluid col-lg-9 col-md-8">
			<div class="row">


				<table class="table table-sm table-striped">
					<thead>
						<tr>
							<th>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="ubnt-all-checkbox" onChange="ubnt.all_checkbox_check()" >
									<label class="form-check-label" for="ubnt-all-checkbox">
										<i class="far fa-arrow-alt-circle-down"></i>
									</label>
								</div>
							</th>
							<th>Имя</th>
							<th>Модель</th>
							<th>IP</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php
foreach ($var["hardware"]["list"] as $row){
	$ip_norm = str_replace('.', '_', $row["ip"]);
?>
						<tr id="ubnt-<?=$ip_norm?>" class="">
							<td>
								<div class="form-check">
									<input class="ubnt-checkbox form-check-input" type="checkbox" value="<?=$ip_norm?>" id="ubnt-<?=$ip_norm?>-checkbox"
										data-id="<?=$row["id"]?>"
										data-ip="<?=$row["ip"]?>"
										data-ip_norm="<?=$ip_norm?>"
										data-name="<?=$row["name"]?>"
										data-model_name="<?=$row["model_name"]?>"
									>
									<input type="number" min="0" max="255" class="form-control form-control-sm py-0" id="ubnt-<?=$ip_norm?>-reboot_priority" value="<?=$row["reboot_priority"]?>" onChange="ubnt.update_one(<?=$row["id"]?>, 'reboot_priority', this.value)" size="3" />
								</div>
							</td>
							<td>
								<button type="button" id="ubnt-<?=$ip_norm?>-online" class="ubnt-online btn btn-light btn-sm py-0" onClick="ubnt.search_one('<?=$row["ip"]?>')">
									<i class="fas fa-bullseye text-light"></i>
								</button>
								<a href="http://<?=$row["ip"]?>/login.cgi?uri=/" target="_blank">
									<?=$row["name"]?>
								</a>
							</td>
							<td>
								<img src="<?=IMG_WEBDIR.'/model/'.$row["model_name"]?>.png" width="25" height="25" />
								<?=$row["model_name"]?>
							</td>
							<td><?=$row["ip"]?></td>
							<td><button type="button" class="btn btn-danger btn-sm py-0" id="ubnt-<?=$ip_norm?>-reboot" onClick="ubnt.reboot_one('<?=$row["ip"]?>')">reboot</button></td>
						</tr>
<?php
}
?>
					</tbody>
				</table>

			</div>
		</main>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#ubnt-all-checkbox').prop('checked', false);
	ubnt.all_checkbox_check();
	ubnt.search();
});
</script>