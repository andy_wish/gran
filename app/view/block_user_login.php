<?php if (!defined("INBOX")) die('separate call');

if(isset($_GET["auth_error"])){
	switch ($_GET["auth_error"]) {
		case 'login_error':
			$auth_error='Неправильные логин или пароль.';
			break;
		case 1:
			$auth_error='Ошибка ввода данных.';
			break;
		case 'logout_error':
			$auth_error='Ошибка выхода.';
			break;
		case 3:
			$auth_error='Ошибка. Подмена запроса.';
		default:
			$auth_error='Ошибка.';
	}
	echo '<span class="badge badge-danger">'.$auth_error.'</span>';
}

if(User::id()){
?>
<form action="/user/logout" method="post" class="form-inline my-2 my-lg-0">
	<div class="input-group">
		<div class="input-group-prepend">
			<div class="input-group-text"><strong><?=User::name()?></strong><!--&nbsp;:&nbsp;<span class="text-muted"><?=User::group_name()?></span>--></div>
		</div>
		<button type="submit" class="btn btn-outline-light">выйти&nbsp;<i class="fas fa-sign-out-alt"></i></button>
	</div>
</form>

<?php 
}else{
?>
<style>
#login_content{
	display: none;
	position: absolute;
	right:1rem;
	top:3rem;
	z-index: 999;
	background: #e2f1fc;
	background: linear-gradient(#38d, #159);
	padding: 11px;
	box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
	border-radius: 3px 0 3px 3px;
	#16b, #38d;
}
</style>
<div class="btn-group-sm" role="group">
	<button type="button" class="btn btn-outline-light" id="login_trigger">Войти <span>&#x25BC;</span></button>
	<!--<a href="/user/registration/" class="btn btn-secondary">Регистрация</a>-->
</div>
<div style="display:none;" id="login_content">
	<form action="/user/login/" method="post">
		<input type="hidden" name="page_current" value="<?=$_SERVER['REQUEST_URI']?>" />
	
		<div class="form-group">
			<label class="sr-only" for="user_email">E-mail</label>
			<div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-at"></i></div>
				</div>
				<input type="email" name="user_email" class="form-control" id="user_email" placeholder="e-mail" required />
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="user_password">Пароль</label>
			<div class="input-group mr-sm-2 mb-sm-0">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-key"></i></div>
				</div>
				<input type="password" name="user_password" class="form-control" id="user_password" placeholder="пароль" required />
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="user_password">Пароль</label>
			<div class="input-group mr-sm-2 mb-sm-0">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-sign-in-alt"></i></div>
				</div>
				<button type="submit" class="form-control btn btn-primary">вперёд</button>
			</div>
		</div>

	</form>
</div>                     

<?php
}
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#login_trigger').click(function(){
		$('#login_content').slideToggle();
		$('#login_trigger').toggleClass('active');
		$('#user_email').focus();

		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
			else $(this).find('span').html('&#x25BC;')
	})
});
</script>