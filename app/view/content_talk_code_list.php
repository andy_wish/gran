<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
	</div>
</div>

<div class="container" id="talk_list">
	<div class="row">
		<div class="col-md-8">
			<table class="table table-striped table-sm">
<?php
foreach($var["talk"]["codes"] as $row){
?>
				<tr>
					<td class="text-right"><b><?=$row["code"]?></b></td>
					<td class="text-left py-1">
						<input type="text" class="form-control form-control-sm py-0" id="code_<?=$row["code"]?>_name" value="<?=$row["name"]?>" style="font-size: 120%"  onKeyUp="basis.update_one(<?=$row["id"]?>, 'name', this.value)" />
					</td>
				</tr>
<?php
}
?>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript">
	//var tbl = XLSX.utils.table_to_book(document.getElementById('talk_list_table'), {raw:true});
	//XLSX.writeFile(tbl, output_filename, {cellStyles:true});
</script>