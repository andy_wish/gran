<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
	</div>
</div>

<div class="container" id="talk_list">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-sm">
<?php
//var_dump($var["person"]["list"]);exit;
foreach($var["person"]["radio"]["list"] as $row){
?>
				<tr>
					<td style="width:4.5rem" class="text-right"><b><?=$row["name"]?></b></td>
					<td style="width:18rem" class="text-left py-1">
						<select id="person-radio-select" class="form-control form-control-sm" onChange="person.update_radio(this.value, '<?=$row["name"]?>')">
<?php
$found_radio = false;
foreach($var["person"]["list"] as $person){
?>
							<option value="<?=$person["id"]?>"
<?php
	if($person["radio"] == $row["name"]){
		$found_radio = true;
		echo ' selected';
	}
?>
							><?=$person["surname"].' '.$person["name"].' '.$person["patronymic"]?></option>
<?php
}
if(!$found_radio) echo '<option value="0" selected>не выдана</option>';
?>
					</select>
					</td>
					<td style="min-width:15rem"><input type="text" class="form-control form-control-sm" id="radio_<?=$row["id"]?>_content" value="<?=$row["content"]?>" onKeyUp="basis.update_one(<?=$row["id"]?>, 'content', this.value)" /></td>
					<td><?=$row["datetime"]?></td>
				</tr>
<?php
}
?>
			</table>

		</div>
	</div>
</div>

<script type="text/javascript">
	//var tbl = XLSX.utils.table_to_book(document.getElementById('talk_list_table'), {raw:true});
	//XLSX.writeFile(tbl, output_filename, {cellStyles:true});
</script>