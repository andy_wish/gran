<?php if (!defined("INBOX")) die('separate call');?>
<!doctype html><html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Панель управления">
	<meta name="author" content="umdi.ru">

<link rel="apple-touch-icon" sizes="152x152" href="/app/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/app/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/app/img/favicon/favicon-16x16.png">
<link rel="manifest" href="/app/img/favicon/site.webmanifest">
<link rel="shortcut icon" href="/app/img/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#ffc40d">
<meta name="msapplication-config" content="/app/img/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

	<title><?=isset($var["page"]["title"])? $var["page"]["title"] : 'GranBox'?></title>

	<link href="/app/plugin/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/app/plugin/fontawesome/css/fontawesome-all.css" rel="stylesheet" type="text/css" />
	<link href="/app/plugin/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="/app/css/style.css" rel="stylesheet" type="text/css" />

	<script src="/app/plugin/jquery-3.4.1.min.js" type="text/javascript"></script>
	<script src="/app/plugin/bootstrap/bootstrap.min.js" type="text/javascript"></script>
	<!--<script src="/app/plugin/ckeditor/ckeditor.js" type="text/javascript"></script>-->
	<script src="/app/plugin/moment-with-locales.min.js" type="text/javascript"></script>
	<script src="/app/plugin/datatables/datatables.js" type="text/javascript"></script>
	<script src="/app/plugin/jsonrpc.js" type="text/javascript"></script>
	<script src="/app/js/core.js" type="text/javascript"></script>
	<script src="/app/js/basis.js" type="text/javascript"></script>
	<script src="/app/js/ubnt.js" type="text/javascript"></script>
	<script src="/app/js/person.js" type="text/javascript"></script>
	<script src="/app/js/search.js" type="text/javascript"></script>

</head>

<body>
<header>
	<nav class="navbar navbar-dark navbar-expand-md px-md-5 px-sm-3 px-xs-1" style="background:#16638b;background:linear-gradient(to top, #16b, #38d);font-size:120%">
		<a class="navbar-brand" href="/">GranBox</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarCollapse">

			<ul class="navbar-nav mr-auto nav nav-pills">
<?php
if(User::id()){//test

	$nav=[
		"index"=>[
			"name"=>'Главная',
			"url"=>'/',
			"controller" => 'index',
			"action" => 'index'
		],
		"person"=>[
			"name"=>'Персонал',
			"url"=>'/person/',
			"controller" => 'person',
			"action" => 'index'
		],
		"talk"=>[
			"name"=>'Время разговора',
			"url"=>'/talk/',
			"controller" => 'talk',
			"action" => 'index'
		],
		"radio"=>[
			"name"=>'Рации',
			"url"=>'/person/radio',
			"controller" => 'person',
			"action" => 'radio'
		],
		"hardware"=>[
			"name"=>'WiFi',
			"url"=>'/hardware',
			"controller" => 'hardware',
			"action" => 'index'
		],
		"sign_separate"=>[],//знак разделителя
		"user"=>[
			"name"=>'Users',
			"url"=>'/user',
			"controller" => 'user',
			"action" => 'index'
		],
	];
	foreach($nav as $current=>$val){
		if($current != 'sign_separate'){

			if(!Access::permit($val["controller"], $val["action"])) continue;

			if($var["page"]["current"]==$current) $active_mark=' active';
			else $active_mark='';
?>
				<li class="nav-item<?=$active_mark?>">
					<a class="nav-link" href="<?=$val["url"]?>"><?=$val["name"]?></a>
				</li>
<?php
		}else{
?>
				<li class="nav-item">&emsp;&emsp;</li>
<?php
		}
	}
?>
			<!--<li class="nav-item">
					<span class="badge badge-secondary" id="search_count_box" style="float:right"></span>
					<button OnClick="search_query_delete()" id="search_top_query_delete_btn" class="btn btn-sm btn-secondary p-0 px-1 mr-1 mt-1" style="float:right;margin-left:-0.4rem;display:none" title="убрать поисковую строку"><i class="fas fa-window-close"></i></button>
					<input id="search_box" class="form-control form-control-sm mr-sm-2" type="text" autocomplete="off" placeholder="поиск" value="" maxlength="20" style="width:23rem" OnKeyUp="search_top_main(event.keyCode)" OnClick="search_top_main('click')" />
					<table class="table table-sm table-striped" id="search_list"></table>
				</li>-->
<?php
}
?>
			</ul>

			<?php require 'block_user_login.php';?>

		</div>
	</nav>
<?php
if(isset($var["success"])){//success
?>
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<?=$var["success"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
if(isset($var["error"])){//error
?>
	<div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
		<?=$var["error"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
if(isset($var["warning"])){//warning
?>
	<div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
		<?=$var["warning"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
?>
</header>