<?php if (!defined("INBOX")) die('separate call');?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item active">Главная</li>
			</ol>
		</div>
	</div>
</div>

<?php
if(User::id()){
?>
<div class="container">

	<div class="row justify-content-center">
		<div class="col-md-12">
			<h1><?=$var["page"]["h1"]?></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-7">
		</div>
		<div class="col-md-5">
		</div>
	</div>
	
</div>




<script type="text/javascript">

var domains_my_id = 'domains_my';
var domains_other_id = 'domains_other';
var user_id = <?=User::id()?>

$(document).ready(function(){

	wait.start()

	/*var domains = basis.read({
		type: 'domain',
		closed: 1
	});
	if(typeof(domains.error)!=='undefined'){
		$('#'+domains_my_id).html(domains.error.msg);
		return
	}

	$('#'+domains_my_id).empty();
	var table_domains_my_id = basis.draw({
		method: 'table',
		//style: {width: '12rem', borderTop: '2px solid #777'},
		title:[
			{name: '№'},
			{name: 'Порядок вывода'},
			{name: ''},
			{name: 'Название'},
			{name: 'дата'},
			{name: 'статус'}
		],
		target: domains_my_id,
	});

	$('#'+domains_other_id).empty();
	var table_domains_other_id = basis.draw({
		method: 'table',
		//style: {width: '10rem', borderTop: '4px solid #333'},
		title:[
			{name: '№'},
			{name: ''},
			{name: 'Название'},
			{name: 'дата'},
			{name: 'статус'}
		],
		target: domains_other_id,
	});

	$.each(domains.result.list, function(item, value){

		if(this.user_id!=user_id) return true;

		basis.draw({
			method: 'table_row',
			place: 'bottom',
			data: this,
			id: this.id,
			list:[
				{
					name: 'id',
					value: this.id,
					style: {width: '1rem'},
					editable: false
				},{
					name: 'order_num',
					value: this.order_num,
					style: {width: '3rem'},
					editable: true
				},{
					name: 'img',
					value: this.img,
					editable: true
				},{
					name: 'name',
					value: this.name,
					editable: true
				},{
					name: 'created_nice',
					value: this.created_nice,
					editable: false
				},{
					name: 'closed',
					value: this.closed,
					editable: true
				},
			],
			target: table_domains_my_id,
		});
	});

	$.each(domains.result.list, function(item, value){

		if(this.user_id==user_id) return true;

		basis.draw({
			method: 'table_row',
			place: 'bottom',
			data: this,
			list:[
				{
					name: 'id',
					value: this.id,
					style: {width: '1rem'},
					editable: false
				},{
					name: 'img',
					value: this.img,
					editable: false
				},{
					name: 'name',
					value: this.name,
					editable: false
				},{
					name: 'created_nice',
					value: this.created_nice,
					editable: false
				},{
					name: 'closed',
					value: this.closed,
					editable: false
				},
			],
			target: table_domains_other_id,
		});
	});*/

	wait.finish()
});
</script>
<?php
}else{
?>
<div class="container p-4" style="max-width:30rem">
	<h4>Необходимо войти в систему</h4>
</div>
<?php
}
?>