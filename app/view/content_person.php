<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<?php include 'app/view/block_breadcrumb.php';?>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		<div class="col-md-4">
			<img src="/files/image/<?=$var["person"]["img"]?>" id="person_<?=$var["person"]["id"]?>_img" title="/files/image/<?=$var["person"]["img"]?>" style="width:200px" id="person_<?=$var["person"]["id"]?>_img" />
		</div>

		<div class="col-md-8">
			<div class="form-row">
				<div class="form-group col-md-4" title="id <?=$var["person"]["id"]?>">
					<label for="person-<?=$var["person"]["id"]?>-surname">Фамилия</label>
					<input type="text" class="form-control" id="person-<?=$var["person"]["id"]?>-surname" value="<?=$var["person"]["surname"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'surname', this.value)" />
				</div>
				<div class="form-group col-md-4">
					<label for="person-<?=$var["person"]["id"]?>-name">Имя</label>
					<input type="text" class="form-control" id="person-<?=$var["person"]["id"]?>-name" value="<?=$var["person"]["name"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'name', this.value)" />
				</div>
				<div class="form-group col-md-4">
					<label for="person-<?=$var["person"]["id"]?>-patronymic">Отчество</label>
					<input type="text" class="form-control" id="person-<?=$var["person"]["id"]?>-patronymic" value="<?=$var["person"]["patronymic"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'patronymic', this.value)" />
				</div>

				<div class="form-group col-md-8">
					<label for="person-position-select">Должность</label>
					<select id="person-position-select" class="form-control" onChange="person.update_one(<?=$var["person"]["id"]?>, 'position', this.value)">
<?php
foreach($var["position"]["list"] as $row){
?>
						<option value="<?=$row["id"]?>"
<?php
	if($var["person"]["position"] == $row["id"]) echo ' selected';
?>
						><?=$row["name"]?></option>
<?php
}
?>
					</select>
				</div>

				<div class="form-group col-md-4">
					<label for="person-place-select">Место работы</label>
					<select id="person-place-select" class="form-control" onChange="person.update_one(<?=$var["person"]["id"]?>, 'place', this.value)">
<?php
foreach($var["place"]["list"] as $row){
?>
						<option value="<?=$row["id"]?>"
<?php
	if($var["person"]["place"] == $row["id"]) echo ' selected';
?>
						><?=$row["name"]?></option>
<?php
}
?>
					</select>
				</div>
			</div>
<?php
/*echo '<pre>';
var_dump($var["person"]);
echo '</pre>';
*/
?>

		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="person-<?=$var["person"]["id"]?>-surname">телефон</label>
				<input type="text" class="form-control form-control" id="person_<?=$var["person"]["id"]?>_phone" value="<?=$var["person"]["phone"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'phone', this.value)" />
			</div>
			<div class="form-group">
				<label for="person-<?=$var["person"]["id"]?>-name">Адрес</label>
				<input type="text" class="form-control form-control" id="person_<?=$var["person"]["id"]?>_address" value="<?=$var["person"]["address"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'address', this.value)" />
			</div>
			<div class="form-group">
				<label for="person-<?=$var["person"]["id"]?>-patronymic">Рация</label>
				<input type="text" class="form-control form-control" id="person_<?=$var["person"]["id"]?>_radio" value="<?=$var["person"]["radio"]?>" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'radio', this.value)" />
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="person-<?=$var["person"]["id"]?>-description">Примечание</label>
				<textarea class="form-control form-control" id="person_<?=$var["person"]["id"]?>_description" onKeyUp="person.update_one(<?=$var["person"]["id"]?>, 'description', this.value)"><?=$var["person"]["description"]?></textarea>
			</div>
		</div>
	
	</div>
</div>