<?php if (!defined("INBOX")) die('separate call');

class Hardware_controller extends Controller {

	function __construct($var) {
		$this->model=new Hardware_model();
		$this->view=new View();
		$var["page"]["current"]='hardware';

		if(isset($var[0])){
			switch($var[0]){
				case 'code':
					$action='code_list';
					break;
				default:
					Core::error(404);
			}
		}else{
			$action = 'index';
		}

		if(Access::permit('hardware', $action)) $this->$action($var);
		else Core::error(401);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_hardware_list.php', $var);
	}

	function code_list($var) {
		$var=$this->model->code_list($var);
		$this->view->generate('template.php', 'content_hardware_code_list.php', $var);
	}

}
