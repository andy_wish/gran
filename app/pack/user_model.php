<?php if (!defined("INBOX")) die('separate call');

class User_Model extends Model {

	public function index($var) {

		//PAGE
		$var["page"]["h1"] = $var["page"]["title"] = 'Users';

		$var["user"] = User::read([
			"closed" => 2,
			"response" =>[
				"limit" => 450,
				"order" => 'name',
				"direction" => 'asc'
			]
		]);

		//var_dump($var["user"]);exit;

		//BREADCRUMB
		$var["breadcrumb"]=[];

		return $var;
	}

	public function read($var) {

		$var["user"] = User::read([
			"id" => $var[1],
			"closed" => 2 //all
		]);

		if(!$var["user"]) Core::error(404);
		$var["user"] = $var["user"]["list"][0];

		$var["group"]["list"] = DB::getAll('SELECT * FROM `user_group` ORDER BY `id`');

		//PAGE
		$var["page"]["h1"]='редактировать';
		$var["page"]["title"]='редактировать';
		//$var["page"]["img"]=$var["action"]["img"];

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/user/',"name"=>'users']];

		return $var;
	}

	/*public function registration_action($var) {

		if(
			(
			isset($var["post"]["user_email"]) AND
			isset($var["post"]["user_password"]) AND
			isset($var["post"]["user_password2"]) AND
			isset($var["post"]["user_name"]) AND
			isset($var["post"]["user_telephone"])
			) AND
		
			isset($var["post"]["form_token"]) AND
			isset($var["post"]["form_action"]) AND
			$var["post"]["form_token"]!='false' AND
			$var["post"]["form_action"]=='user_registration_submit' AND
			$var["post"]["user_password"]==$var["post"]["user_password2"]
		){

			if(Recaptcha::check($var["post"]["form_token"], $var["post"]["form_action"])){

				$var["result"]=User::registration_start([
					"email"=>$var["post"]["user_email"],
					"password"=>$var["post"]["user_password"],
					"password2"=>$var["post"]["user_password2"],
					"name"=>$var["post"]["user_name"],
					"phone"=>$var["post"]["user_telephone"]
				]);

			}else $var["result"]["error"]='registration data error';

		}else $var["result"]["error"]='registration data error';

		if(!isset($var["result"]["error"])) {
			$var["action"]='check_email';
			$var["page"]["title"]='Проверка почты';
		}else {
			$var["action"]='registration_error';
			$var["page"]["title"]='Ошибка при регистрации';
		}

		return $var;
	}*/


}
