<?php if (!defined("INBOX")) die('separate call');

class Files_model extends Model {

	public function create_ajax($var) {

		//var_dump($_POST);
		//var_dump($_FILES);
		/*array (size=1)
		0 => 
			array (size=5)
			  'name' => string '1.jpg' (length=5)
			  'type' => string 'image/jpeg' (length=10)
			  'tmp_name' => string 'C:\OSPanel\userdata\temp\phpC700.tmp' (length=36)
			  'error' => int 0
			  'size' => int 416290
		*/
		$result=[
			"total"=>0,
			"list"=>[]
		];
		$i=-1;

		foreach($_FILES as $file){
			$i++;
			$result["total"]++;
			$result["list"][$i]=Files::upload($file);
		}

		echo json_encode($result);
		exit;

	}

	public function update_ajax($var) {
		//var_dump($_POST);
		if(!isset($_POST['id'])) die(json_encode(array('error'=> 'id required')));

		$_POST["id"]=(int)$_POST["id"];

		$answer=Files::update($_POST);
		echo json_encode($answer);
		exit;
	}

	public function delete_ajax($var) {
		//var_dump($_POST);
		if(!isset($_POST['id'])) die(json_encode(array('error'=> 'id required')));
		if(!isset($_POST['type'])) die(json_encode(array('error'=> 'type required')));
		if(!isset($_POST['src'])) die(json_encode(array('error'=> 'src required')));

		$answer=Files::delete(array("id"=> (int)$_POST["id"]));
		if(!isset($answer["error"])){
			if (file_exists(ROOT_DIR.'/files/'.$_POST['type'].'/'.$_POST['src'])) {
				unlink (ROOT_DIR.'/files/'.$_POST['type'].'/'.$_POST['src']);
			}else die(json_encode(array('error'=> 'file not exist')));
			
		}

		echo json_encode($answer);
		exit;
	}

	public function upload_ckeditor($var) {

		//var_dump($_POST);
		//var_dump($_FILES);
		//var_dump($_REQUEST);
		//var_dump($var);
		if(
			!isset($var["cookie"]["ckCsrfToken"]) OR
			!isset($var["post"]["ckCsrfToken"]) OR
			$var["cookie"]["ckCsrfToken"] != $var["post"]["ckCsrfToken"]
		) die('CSRF?');

		//header('POST '.$var["post"]["ckCsrfToken"]);
		//setcookie('ckCsrfToken', $var["cookie"]["ckCsrfToken"]);

		$result=[
			"total"=>0,
			"list"=>[]
		];
		$i=-1;

		foreach($_FILES as $file){
			$i++;
			$result["total"]++;
			$result["list"][$i]=Files::upload($file);
		}

		/*$callback = $_REQUEST['CKEditorFuncNum'];
		echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'.$callback.'", "'.$result["list"][0]["src_full"].'", "Загружено '.$result["list"][0]["size_nice"].'" );</script>';
		$output = '<html><body><script type="text/javascript">window.parent.CKEDITOR.tools.callFunction('.$callback.', "'.$result["list"][0]["src_full"].'", "");</script></body></html>';
		echo $output;*/

		echo json_encode([
			"uploaded" => 1,
			"fileName" => $result["list"][0]["src"],
			"url" => '/files/image/'.$result["list"][0]["src"],
			//"error" => ["message" => 'Загружено '.$result["list"][0]["size_nice"]],
		]);

		exit;

	}

}