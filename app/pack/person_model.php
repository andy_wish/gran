<?php if (!defined("INBOX")) die('separate call');

class Person_model extends Model {

	public function index($var) {

		//PAGE
		$var["page"]["h1"] = $var["page"]["title"] = 'Персонал';

		$params = [
			"closed" => 2,
			"response" =>[
				"limit" => 450,
				"order" => 'surname',
				"direction" => 'asc'
			]
		];
		if(isset($var["get"]["place"]) AND strlen($var["get"]["place"]) >= 3){
			switch($var["get"]["place"]){
				case 'anich':
					$params["place"] = 38;
					$var["page"]["title"] .= ', Анич';
					$var["page"]["h1"] .= ', Анич';
					break;
				case 'dusk':
					$params["place"] = 39;
					$var["page"]["title"] .= ', Дусканья';
					$var["page"]["h1"] .= ', Дусканья';
					break;
				case 'ustom':
					$params["place"] = 40;
					$var["page"]["title"] .= ', Усть-Омчуг';
					$var["page"]["h1"] .= ', Усть-Омчуг';
					break;
			}
		}
		//var_dump($params);exit;

		$var["person"] = Person::read($params);



		//BREADCRUMB
		$var["breadcrumb"]=[];

		return $var;
	}

	public function radio($var) {

		//PAGE
		$var["page"]["h1"] = $var["page"]["title"] = 'Рации';

		$var["person"] = Person::read([
			"response" =>[
				"limit" => 5000,
				"order" => 'surname'
			]
		]);

		$var["person"]["radio"] = Basis::read([
			//"closed" => 2,
			"type" => 'radio',
			"response" =>[
				"limit" => 500,
				"order" => 'name',
				"direction" => 'asc'
			]
		]);


		
/*		$radio = [];
		foreach($var["person"]["radio"]["list"] as $row){
			$new = [
				"id" = $row["id"],
				"name" = $row["name"],
				"content" = $row["content"],
				"datetime" = $row["datetime"],
				
			]
			$radio[] = 
		}*/


		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/person',"name"=>'Персонал']];

		return $var;
	}

	public function read($var) {

		$var["person"] = Person::read([
			"id" => $var[1],
			//"closed" =>2 //all action
		]);

		if(!$var["person"]) Core::error(404);
		$var["person"] = $var["person"]["list"][0];

		$var["place"] = Basis::read([
			"type" => 'place',
			"response" =>[
				"order" => 'name',
				"limit" => 500
			]
		]);

		$var["position"] = Basis::read([
			"type" => 'position',
			"response" =>[
				"order" => 'name',
				"limit" => 500
			]
		]);
		

		//PAGE
		$var["page"]["h1"]='редактировать';
		$var["page"]["title"]='редактировать';
		//$var["page"]["img"]=$var["action"]["img"];

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/person/',"name"=>'Персонал']];

		return $var;
	}

	public function update($var) {//

		if(isset($var[1])) $params["id"]=(int)$var[1];
		else {
			$var["error"]='id required';
			return $var;
		}

		if(isset($var["post"]["name"])) $params["name"]=$var["post"]["name"];
/*		else {
			$var["error"]='name required';
			return $var;
		}*/

		if(isset($var["post"]["content"])) $params["content"]=$var["post"]["content"];
		if(isset($var["post"]["datetime"])) $params["datetime"]=$var["post"]["datetime"];

//var_dump($params);exit;
		$result = Person::update($params);
//var_dump($result);exit;
		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}

		$var["person"]=Person::read([
			"id"=>$var[1],
			"closed"=>2//all persons
		]);

		if(!$var["person"]) Core::error(404);
		$var["person"]=$var["person"]["list"][0];

		//PAGE
		$var["page"]["h1"]='редактировать';
		$var["page"]["title"]='редактировать';
		//$var["page"]["img"]=$var["person"]["img"];

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/admin/person/',"name"=>'Персонал']];

		return $var;
	}

}
