<?php if (!defined("INBOX")) die('separate call');

class User_Controller extends Controller{

	function __construct($var){
		$this->model = new User_Model();
		$this->view = new View();

		$var["page"]["current"]='user';

		if(isset($var[0])){
			switch($var[0]){
				/*case 'setting':
					$action='setting';
					break;*/
				case 'login':
					$action='login';
					break;
				case 'logout':
					$action='logout';
					break;
				case 'read':
					$action='read';
					break;
				/*case 'registration':
					$action='registration';
					break;
				case 'registration_action':
					$action='registration_action';
					break;
				case 'activate':
					$var["page"]["current"]='registration_action';
					$action='activate';
					break;
				case 'auth_ulogin':
					$action='auth_ulogin';
					break;
				case 'access_recovery_form':
					$action='access_recovery_form';
					break;
				case 'access_recovery_action':
					$action='access_recovery_action';
					break;*/
				default:
					Core::error(404);
			}
		}else{
			if(User::id()){
				//Core::error(404);
				$action = 'index';
			}else{
				$action = 'registration';
			}
		}

		if(!Access::permit($var["page"]["controller"], $action)) Core::error(401);
		else $this->$action($var);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_user_list.php', $var);
	}

	function read($var) {
		$var = $this->model->read($var);
		$this->view->generate('template.php', 'content_user.php', $var);
	}

	/*function index($var){
		$var["user"]=User::info();
		$var=$this->model->index($var);
		$this->view->generate('template_main.php', 'content_user.php', $var);
	}

	function access_recovery_form($var){
		//$var["user"]=User::access_recovery();
		//$var=$this->model->index($var);
		$this->view->generate('template_main.php', 'content_user_access_recovery_form.php', $var);
	}

	function access_recovery_action($var){
		$var["result"]=User::access_recovery_action([
			"user_id"=>$var["post"]["u"],
			"secretkey"=>$var["post"]["k"],
			"password"=>$var["post"]["user_password"],
			"password2"=>$var["post"]["user_password2"],
			"token"=>$var["post"]["form_token"],
			"action"=>$var["post"]["form_action"]
		]);
		if(!isset($var["result"]["error"])) $var["action"]='success';
		else $var["action"]='error';
		
		//$var=$this->model->index($var);
		$this->view->generate('template_main.php', 'content_user_access_recovery_action.php', $var);
	}

	function setting($var){
		$var["user"]=User::info();
		$this->view->generate('template_main.php', 'content_user_setting.php', $var);
	}*/

	function login($var){

		if(!isset($var["post"]["user_email"]) OR !isset($var["post"]["user_password"])){
			//$var["error"]='login data error';
			//return $var;
			Core::error(449, 'login and password required');
		}

		$params=User::login([
			"email"=>$var["post"]["user_email"],
			"password"=>$var["post"]["user_password"]
		]);

		if(isset($params["error"])){
			$location='/?auth_error='.$params["error"];
			header('Location: '.$location);
			exit();
		}else{
			SetCookie('data', $params["token_id"].':'.$params["token_str"], $params["token_expired"], '/');
		}

		header('Location: /');
		exit();
	}

	function logout($var){

		if(isset($var["cookie"]["data"])){
			$t=explode(':', $var["cookie"]["data"]);
			$result=User::logout([
				"token_id"=>$t[0],
				"token_str"=>$t[1]
			]);
		}
		SetCookie('data', '', time(), '/');
		header('Location: /');
		exit();
	}

	/*function registration($var){

		//PAGE
		$var["page"]["title"]='Регистрация';
		$var["page"]["description"]=$var["page"]["title"];
		$var["page"]["keywords"]=$var["page"]["title"];
		$var["page"]["h1"]=$var["page"]["title"];

		//BREADCRUMB
		$var["breadcrumb"]=[];

		$this->view->generate('template_main.php', 'content_user_registration.php', $var);
	}

	function registration_action($var){

		$var=$this->model->registration_action($var);

		//PAGE
		$var["page"]["description"]=$var["page"]["title"];
		$var["page"]["keywords"]=$var["page"]["title"];
		$var["page"]["h1"]=$var["page"]["title"];

		//BREADCRUMB
		$var["breadcrumb"]=[
			["url"=>'/user/registration/', "name"=>'регистрация'],
		];

		$this->view->generate('template_main.php', 'content_user_registration_action.php', $var);
	}

	function activate($var){
		if(isset($var["get"]["u"]) AND isset($var["get"]["k"])){
			$var["result"]=User::email_activate([
				"id"=>$var["get"]["u"],
				"actkey"=>$var["get"]["k"]
			]);
		}else Core::error(404);

		if(!isset($var["result"]["error"])){
			$var["page"]["title"]='Успешно проверили почту';
			$var["action"]='activation_success';
		}else{
			$var["page"]["title"]='Ошибка';
			$var["action"]='activation_error';
		}

		//PAGE
		$var["page"]["description"]=$var["page"]["title"];
		$var["page"]["keywords"]=$var["page"]["title"];
		$var["page"]["h1"]=$var["page"]["title"];

		//BREADCRUMB
		$var["breadcrumb"]=[
			["url"=>'/user/registration/', "name"=>'регистрация'],
		];
		
		$this->view->generate('template_main.php', 'content_user_registration_action.php', $var);
	}

	function auth_ulogin($var){
		(!isset($var["get"]["src"]) OR $var["get"]["src"]=='')? $redirect='/' : $redirect=$var["get"]["src"];

		$var=User::auth_ulogin($var);
		if(isset($var["error"])){
			$location='/?auth_error='.$var["error"];
			header('Location: '.$location);
			exit();
		}

		//var_dump($var["ulogin"]);exit;
		$var["login_result"]=User::login([
			"email"=>$var["ulogin"]["login"],
			"password"=>$var["ulogin"]["password"]
		]);

		if(isset($var["login_result"]["error"])){
			$location=$redirect.'?auth_error='.$var["login_result"]["error"];
			header('Location: '.$location);
			exit();
		}else{
			SetCookie('data', $var["login_result"]["token_id"].':'.$var["login_result"]["token_str"], $var["login_result"]["token_expired"], '/');
		}

		header('Location: '.$redirect);
		exit();

	}*/

}
