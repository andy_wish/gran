<?php if (!defined("INBOX")) die('separate call');

class Person_controller extends Controller {

	function __construct($var) {
		$this->model = new Person_model();
		$this->view = new View();
		$var["page"]["current"] = 'person';

		if(isset($var[0])) {
			switch($var[0]) {

				case 'read':
					$action='read';
					break;
				case 'radio':
					$action='radio';
					break;
				default:
					Core::error(404);
			}
		} else {
			$action='index';
		}

		if(Access::permit('person', $action)) $this->$action($var);
		else Core::error(401);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_person_list.php', $var);
	}

	function radio($var) {
		$var=$this->model->radio($var);
		$this->view->generate('template.php', 'content_person_radio_list.php', $var);
	}

	function read($var) {
		$var = $this->model->read($var);
		$this->view->generate('template.php', 'content_person.php', $var);
	}

}
