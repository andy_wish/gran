<?php if (!defined("INBOX")) die('separate call');

class Talk_controller extends Controller {

	function __construct($var) {
		$this->model=new Talk_model();
		$this->view=new View();
		$var["page"]["current"]='talk';

		if(isset($var[0])){
			switch($var[0]){
				case 'code':
					$action='code_list';
					break;
				default:
					Core::error(404);
			}
		}else{
			$action = 'index';
		}

		if(Access::permit('talk', $action)) $this->$action($var);
		else Core::error(401);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_talk_list.php', $var);
	}

	function code_list($var) {
		$var=$this->model->code_list($var);
		$this->view->generate('template.php', 'content_talk_code_list.php', $var);
	}

}
