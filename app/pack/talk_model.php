<?php if (!defined("INBOX")) die('separate call');

class Talk_model extends Model {

	public function index($var) {

		$var["talk"] = [];

		//RESULT
		$var["talk"]["insert_result"] = DB::getRow("SELECT `content`, `datetime`, `created`, DATE_FORMAT(`created`, '".Core::config("mysql_date_comment_nice")."') AS `time_finish_nice`, DATE_FORMAT(`datetime`, '".Core::config("mysql_date_comment_nice")."') AS `time_start_nice` FROM `basis` WHERE `type`='talk_insert_result' LIMIT 1");
		$var["talk"]["insert_result"]["temp"] = explode(';', $var["talk"]["insert_result"]["content"]);
		$var["talk"]["insert_result"] = [
			"time_start" => $var["talk"]["insert_result"]["datetime"],
			"time_finish" => $var["talk"]["insert_result"]["created"],
			"time_start_nice" => $var["talk"]["insert_result"]["time_start_nice"],
			"time_finish_nice" => $var["talk"]["insert_result"]["time_finish_nice"],
			"files" => $var["talk"]["insert_result"]["temp"][0],
			"line" => $var["talk"]["insert_result"]["temp"][1],
			"line_insert" => $var["talk"]["insert_result"]["temp"][2]
		];
		$datetimeFrom = DateTime::createFromFormat('Y-m-d H:i:s', $var["talk"]["insert_result"]["time_start"]);
		$datetimeTo = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
		$var["talk"]["insert_result"]["day_ago"] = $datetimeFrom->diff( $datetimeTo )->format('%a');

		//CODES
		$q = "SELECT `name`, `content` FROM `basis` WHERE `type`='phonecode' ORDER BY `name`";
		$codes_db = DB::getAll($q);
		$var["talk"]["codes"] = [];
		foreach($codes_db as $row){
			$var["talk"]["codes"][$row["content"]] = $row["name"];
		}

		//добавить нормальную фильтрацию встроенным фильтром
		if(isset($var["get"]["date_from"]) AND strlen($var["get"]["date_from"]) == 10) $var["talk"]["date_from"] = $var["get"]["date_from"].' 00:00:00';
		//else $var["talk"]["date_from"] = date('Y-m-01 00:00:00');
		else $var["talk"]["date_from"] = date('Y-03-01 00:00:00');

		if(isset($var["get"]["date_to"]) AND strlen($var["get"]["date_to"]) == 10) $var["talk"]["date_to"] = $var["get"]["date_to"].' 23:59:59';
		else{
			$var["talk"]["date_to"] = new DateTime('last day of this month');
			$var["talk"]["date_to"] = $var["talk"]["date_to"]->format('Y-m-d 23:59:59');
		}

		if(isset($var["get"]["name"]) AND strlen($var["get"]["name"]) < 100){
			$var["talk"]["name"] = $var["get"]["name"];

			foreach($var["talk"]["codes"] as $c => $n){
				if($var["talk"]["name"] == $n){
					$code_search = $c;
					break;
				}
			}
		}else $var["talk"]["name"] = 'all';

		$q = "
			SELECT `talk`.*, DATE_FORMAT(`talk`.`datetime`, '%d&nbsp;%M&nbsp;%Y&nbsp;%H:%i:%s') AS `date_nice`, DATE_FORMAT(`talk`.`datetime`, '%d.%m.%y %H:%i:%s') AS `datetime_excel` FROM `talk`
			WHERE `talk`.`datetime`>=?s AND `talk`.`datetime`<=?s";

			if($var["talk"]["name"] !='all') $q .= DB::parse(' AND `talk`.`code`=?s', $code_search);

		$q .= " ORDER BY `talk`.`code` DESC, `talk`.`datetime` ASC";

		//echo $q;exit;
		$var["talk"]["list"] = DB::getAll($q, $var["talk"]["date_from"], $var["talk"]["date_to"]);
		//echo '<pre>';var_dump($var["talk"]);echo '</pre>';

		//PAGE
		$date_from = date_create(substr($var["talk"]["date_from"], 0, 10));
		$date_from = date_format($date_from, "d.m.y");
		$date_to = date_create(substr($var["talk"]["date_to"], 0, 10));
		$date_to = date_format($date_to, "d.m.y");
		
		$var["page"]["h1"] = $var["page"]["title"] = 'Разговоры c '.$date_from.' по '.$date_to;
		
		//BREADCRUMB
		$var["breadcrumb"]=[];

		return $var;
	}

	public function code_list($var) {

		$var["page"]["title"] = 'Телефонные коды';
		$var["page"]["h1"] = 'Телефонные коды';
		$var["talk"] = [];

		$q = "SELECT `id`, `name`, `content` AS `code` FROM `basis` WHERE `type`='phonecode' ORDER BY `content`";
		$var["talk"]["codes"] = DB::getAll($q);

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/talk',"name"=>'Разговоры']];

		return $var;
	}

}
