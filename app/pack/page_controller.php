<?php if (!defined("INBOX")) die('separate call');

class Page_controller extends Controller {

	function __construct($var) {
		//var_dump($var);exit;

		$this->model=new Page_model();
		$this->view=new View();
		$action=str_replace('-', '_', $var["page"]["action"]);
		$var["page"]["current"]=$action;

		//if(!Access::permit($var["page"]["controller"], $var["page"]["action"])) Core::error(401);
		//Cache::start($var["page"]["controller"], $var["page"]["action"]);

		if(!method_exists($this->model, $action)) $var=$this->model->default_page($var);
		else $var=$this->model->$action($var);

		if(!$var["content"]) Core::error(404);


		if(!$var["page"]["mobile_view"]) $m_tag = '';
		else $m_tag = '_m';

		switch($var["page"]["action"]){
			case 'skazka-na-naberezhnoj':
			case 'gruzinskij-zal-skazki':
			case 'chajhana-skazka':
				$template = 'template_main'.$m_tag.'.php';
				$content = 'page_restaurants'.$m_tag.'.php';
				break;

			case 'otzyvy':
				$template = 'template_main'.$m_tag.'.php';
				$content = 'page_otzyvy'.$m_tag.'.php';
				break;

			default:
				$template = 'template_main'.$m_tag.'.php';
				$content = 'page_default'.$m_tag.'.php';

		}

		if(!file_exists(APP_VIEW_DIR.'/'.$content)){
			$this->view->generate($template, 'page_default'.$m_tag.'.php', $var);
		}else{
			$this->view->generate($template, $content, $var);
		}

		//Cache::finish();
	}

}