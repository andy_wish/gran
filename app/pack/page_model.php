<?php if (!defined("INBOX")) die('separate call');

class Page_model extends Model {

	public function default_page($var){

		$var["content"]=Basis::read([
			'type'=>'page',
			'name_translit'=>$var["page"]["action"],
			'response'=>[
				'limit'=>1
			]
		]);
		if($var["content"]["total"] < 1){
			$var["content"] = false;
			return $var;
		}

		$var["content"] = $var["content"]["list"][0];

		//PAGE Заполнение по умолчанию в классе Core, get_page_data()
		$var["page"]["title"] = $var["content"]["name"];
		$var["page"]["h1"] = $var["content"]["name"];
		if($var["content"]["img"]!= '') $var["page"]["img"] = Core::config('site_url').$var["content"]["img"];
		$var["page"]["type"] = 'website'; //website article product
		$var["page"]["url"] = Core::config('site_url').'/'.$var["page"]["action"];
		$var["page"]["description"] = $var["content"]["description"];
		$var["page"]["keywords"] = $var["content"]["keywords"];
		$var["page"]["current"] = '';

		return $var;
	}

	public function chajhana_skazka($var){
		return $this->org($var);
	}
	public function gruzinskij_zal_skazki($var){
		return $this->org($var);
	}
	public function skazka_na_naberezhnoj($var){
		return $this->org($var);
	}

	private function org($var){

		$var["content"] = Basis::read([
			"type"=> 'org',
			"name_translit"=> $var["page"]["action"],
			"meta"=> 'autoload',
			"response"=> [
				"limit"=> 1
			]
		]);
//var_dump($var);exit;

		if($var["content"]["total"]<1){
			$var["content"] = false;
			return $var;
		}else $var["content"] = $var["content"]["list"][0];

		$var["chef"] = Basis::read([
			"id"=> $var["content"]["meta"]["chef"],
			"response"=> [
				"limit"=> 1
			]
		]);
		$var["chef"] = $var["chef"]["list"][0];

		$var["banner_top"] = Basis::read([
			'type'=>'banner_top',
			"meta_get_value"=> 'location',
			'response'=>[
				'order'=>'order_num',
				'direction'=>'asc'
			]
		]);

		$var["news"] = Basis::read([
			"type"=> 'news',
			"meta_get_value"=> 'location',
			"response"=> [
				"order"=> 'order_num',
				"direction"=> 'asc'
			]
		]);
		$var["news"]["filter"]["location"] = $var["content"]["id"];

		$var["action"] = Basis::read([
			"type"=>'action',
			"meta_get_value"=> 'location',
			"response"=>[
				"order"=>'order_num',
				"direction"=>'asc'
			]
		]);
		$var["action"]["filter"]["location"] = $var["content"]["id"];

		$var["interior"] = Basis::read([
			"type"=>'interior',
			"meta_get_value"=> 'location',
			"response"=>[
				"order"=>'order_num',
				"direction"=>'asc'
			]
		]);
		$var["interior"]["filter"]["location"] = $var["content"]["id"];

		$var["comment"] = Basis::read([
			"type"=>'comment',
			"response"=>[
				"order"=>'order_num',
				"direction"=>'asc'
			]
		]);



		//PAGE Заполнение по умолчанию в классе Core, get_page_data()
		$var["page"]["title"] = $var["content"]["title"];
		$var["page"]["h1"] = $var["content"]["title"];
		if($var["content"]["img"]!= '') $var["page"]["img"] = Core::config('site_url').'/files/'.$var["content"]["img"];
		$var["page"]["type"] = 'website'; //website article product
		$var["page"]["url"] = Core::config('site_url').'/o-restorane.php';
		$var["page"]["description"] = $var["content"]["description"];
		$var["page"]["keywords"] = $var["content"]["keywords"];
//var_dump($var);exit;
		return $var;
	}

	public function otzyvy($var){

		$var["content"] = true;

		$var["comment"] = Basis::read([
			"type"=> 'comment',
			//"meta"=> 'autoload',
			"response"=>[
				"order"=>'order_num',
				"direction"=>'asc',
				"limit"=> 100
			]
		]);

		//PAGE Заполнение по умолчанию в классе Core, get_page_data()
		$var["page"]["title"] = 'Отзывы';
		$var["page"]["h1"] = $var["page"]["title"];
		//if($var["content"]["img"]!= '') $var["page"]["img"] = Core::config('site_url').'/files/'.$var["content"]["img"];
		$var["page"]["type"] = 'website'; //website article product
		$var["page"]["url"] = Core::config('site_url').'/otzyvy.php';
		$var["page"]["description"] = $var["page"]["title"];
		$var["page"]["keywords"] = $var["page"]["title"];
//var_dump($var);exit;
		return $var;
	}

}
