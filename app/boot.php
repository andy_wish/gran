<?php if (!defined("INBOX")) die('separate call');
define('APP_DIR', ROOT_DIR.'/app');
define('APP_CORE_DIR', APP_DIR.'/core');
define('APP_PACK_DIR', APP_DIR.'/pack');
define('APP_VIEW_DIR', APP_DIR.'/view');
define('APP_CACHE_DIR', APP_DIR.'/cache');
define('APP_PLUGIN_DIR', APP_DIR.'/plugin');
define('APP_IMG_DIR', APP_DIR.'/img');

define('APP_WEBDIR', ROOT_WEBDIR.'/app');
define('APP_PLUGIN_WEBDIR', APP_WEBDIR.'/plugin');
define('APP_IMG_WEBDIR', APP_WEBDIR.'/img');

DB::init($var["db"]);
unset($var["db"]);

require APP_CORE_DIR.'/model.php';
require APP_CORE_DIR.'/view.php';
require APP_CORE_DIR.'/controller.php';
require APP_CORE_DIR.'/core.php';
$var=Core::init($var);

//require APP_CORE_DIR.'/cache.php';
/*echo password_hash('', PASSWORD_DEFAULT);
exit;*/

isset($_COOKIE["data"])? User::auth($_COOKIE["data"]) : User::auth(false);

require APP_DIR.'/access.php';
Access::init($conf["access"]);

require APP_CORE_DIR.'/route.php';
Route::start($var);