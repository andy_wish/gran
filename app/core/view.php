<?php if (!defined("INBOX")) die('separate call');

class View {

	function generate($template_file, $content_file, $var) {
		require APP_VIEW_DIR.'/'.$template_file;
	}

}