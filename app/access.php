<?php if (!defined("INBOX")) die('separate call');

$conf["access"] = [

"default"		=> [8,9],

"index"=>[
	"index"		=> ['all']
],

"user"=>[
	"default"	=> [9],
	"index"		=> [9],
	"create"	=> [9],
	"read"		=> [9],
	"update"	=> [9],
	"delete"	=> [9],
	"login"		=> ['all'],
	"logout"	=> ['all'],
],

"talk"=>[
	"default"	=> [9]
],

"person"=>[
	"default"	=> [8,9],
	"index"		=> [8,9],
	"read"		=> [8,9],
	"radio"		=> [9],
],

"hardware"=>[
	"default"	=> [9],
],

];