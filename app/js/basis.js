var basis = {

	editors: {},
	pattern: {//

		news:{//
			draw:{

				list:{
					title:['№', '', 'Название', 'дата', 'статус'],
					items:[
						{
							name: 'order_num',
							width: '2rem'
						},
						{
							name: 'img',
							width: '2rem'
						},
						{
							name: 'name',
						},
						{
							name: 'created_nice',
						},
						{
							name: 'closed',
						}
					],
				},

				create:{//
					form:[
						{
							name: 'type',
							type: 'hidden',
							value: 'infobox'
						},
						{
							name: 'id_parent',
							type: 'hidden'
						},
						{
							name: 'name',
							title: 'Имя',
							type: 'text',
							required: true
						},
						{
							name: 'content',
							title: 'Описание',
							type: 'textarea',
							rows: 4
						}
					],
					editors:[
						{name:'content'},
					],
				},

				update:{
					form:[
						{
							name: 'id',
							type: 'hidden'
						},
						{
							name: 'name',
							title: 'Имя',
							type: 'text'
						},
						{
							name: 'content',
							title: 'Описание',
							type: 'textarea',
							rows: 14
						}
					],
					editors:[
						{name:'content'},
					],
				}
			},
		},
	},

	draw_table_row: function (type, action, params){//

		$('#' + type).prepend(
			$('<tr id = "basis_' + params.id + '">')
		)
		$.each(this.pattern[type]["draw"][action]["table"], function(item, value){
			switch(this.name){
				case 'name':
					$('#basis_' + params.id).append(
						$('<td style = "width:15rem">').append(
							$('<textarea class = "form-control" id = "basis_' + params.id + '_name" onKeyUp = "basis.update_one(' + params.id + ', \'name\', this.value)" style = "width:100%;height:6rem">' + params.name + '</textarea>')
						),
					)
				break
				case 'content':
					$('#basis_' + params.id).append(
						$('<td style = "width:15rem">').append(
							$('<textarea class = "form-control" id = "basis_' + params.id + '_content" onKeyUp = "basis.update_one(' + params.id + ', \'content\', this.value)" style = "width:100%;height:6rem">' + params.content + '</textarea>')
						),
					)
				break
				case 'img':
					$('#basis_' + params.id).append(
						$('<td style = "width:12rem" title = "' + params.img + '">').append(
							$('<img src = "/files/image/' + params.img + '" class = "rounded event_img img-fluid" id = "basis_' + params.id + '_img" />'),
							$('<div class = "form-group">').append(
								$('<input type = "file" onChange = "basis.files_update(this.files, \'banner_top\', \'update\', {id: ' + params.id + ', type: \'banner_top\'})" accept = "" class = "form-control-file" />')
							)
						),
					)
				break
				case 'order_num':
					$('#basis_' + params.id).append(
						$('<td style = "width:2rem">').append(
							$('<input class = "form-control" type = "number" min = "1" max = "100" value = "' + params.order_num + '" id = "basis_' + params.id + '_order_num" style = "width:4.5rem" onChange = "basis.update_one(' + params.id + ', \'order_num\', this.value)" />')
						),
					)
				break
				case 'target':
					$('#basis_' + params.id).append(
						$('<td>').append(
							$('<input class = "form-control" type = "text" id = "banner_' + params.id + '_target" onKeyUp = "basis.update_one(' + params.id + ', \'target\', this.value)" style = "width:100%;" value = "' + params.target + '" />')
						),
					)
				break
				case 'status_btn':
					$('#basis_' + params.id).append(
						$('<td style = "width:2rem">').append(
							$('<button id = "basis_' + params.id + '_status_btn" class = "btn btn-sm btn-success py-0 px-1" title = "отключить" onClick = "basis.update_one(' + params.id + ', \'status\', 0)"><i class = "fas fa-toggle-on"></i></button>')
						),
					)
				break

			}

		})

	},

	draw: function (params){

		switch(params.method){

			//TABLE
			case 'table':
				var table_id = params.target + '_table';
				var table_class = ' class = "table table-striped table-bordered"';

				if(typeof(params.place) == 'undefined'){
					$('#' + params.target).append(
						$('<table id = "' + table_id + '"' + table_class + ' />')
					);
				}else{
					if(params.place == 'top'){
						$('#' + params.target).prepend(
							$('<table id = "' + table_id + '"' + table_class + ' />')
						);
					}else{
						$('#' + params.target).append(
							$('<table id = "' + table_id + '"' + table_class + ' />')
						);
					}
				}

				if(typeof(params.style)!== 'undefined') $('#' + table_id).css(params.style);

				$('#' + table_id).append(
					$('<thead id = "' + table_id + '_thead" />').append(
						$('<tr id = "' + table_id + '_thead_tr" />')
					),
					$('<tbody id = "' + table_id + '_tbody" />')
				);
				$.each(params.title, function(item, value){
					if(typeof(this.style) == 'undefined') var style = '';
					else var style = ' style = "' + this.style + '"';
					$('#' + table_id + '_thead_tr').append(
						$('<td' + style + '>' + this.name + '</td>')
					)
				});

				return table_id;
				break;

			//TABLE_ROW
			case 'table_row':

				var row_id = params.target + '_basis_' + params.data.id;
				var row_class = ' class = ""';

				if(typeof(params.place) == 'undefined'){
					$('#' + params.target + ' tbody').append(
						$('<tr id = "' + row_id + '"' + row_class + ' />')
					);
				}else{
					if(params.place == 'top'){
						$('#' + params.target + ' tbody').prepend(
							$('<tr id = "' + row_id + '"' + row_class + ' />')
						);
					}else{
						$('#' + params.target + ' tbody').append(
							$('<tr id = "' + row_id + '"' + row_class + ' />')
						);
					}
				}

				if(typeof(params.style)!== 'undefined') $('#' + row_id).css(params.style);

				$.each(params.list, function(item, value){

						var el_id = row_id + '_basis_' + this.name
						$('#' + row_id).append(
							$('<td id = "' + row_id + '_td_' + this.name + '">')
						);

						switch(this.name){
							case 'order_num':
								if(this.editable){
									var element = '<input class = "form-control" type = "number" min = "1" max = "255" value = "' + this.value + '" id = "' + el_id + '" style = "width:4.5rem" onchange = "basis.update_one(' + params.id + ', \'' + this.name + '\', this.value, \'' + el_id + '\')">';
								}else{
									var element = '<span id = "' + el_id + '">' + this.value + '</span>';
								}
								break;
							case 'img':
								if(this.editable){
									var element = '<img id = "' + el_id + '" src = "/files/image/' + this.value + '" class = "rounded event_img" style = "width:5rem">';
								}else{
									var element = '<img id = "' + el_id + '" src = "/files/image/' + this.value + '" class = "rounded event_img" style = "width:5rem">';
								}
								break;
							case 'closed':
								if(this.value == ''){
									if(this.editable){
										var element = '<button id = "' + el_id + '" class = "btn btn-sm btn-success py-0 px-1" onclick = "basis.update_one(' + params.data.id + ', \'status\', 1, \'' + el_id + '\')" data-closed = "0"><i class = "fas fa-toggle-on"></i></button>';
									}else{
										var element = '<span id = "' + el_id + '"> + </span>';
									}
								}else{
									if(this.editable){
										var element = '<button id = "' + el_id + '" class = "btn btn-sm btn-danger py-0 px-1" onclick = "basis.update_one(' + params.data.id + ', \'status\', 0, \'' + el_id + '\')" data-closed = "1" title = "' + params.data.closed + '"><i class = "fas fa-toggle-on"></i></button>';
									}else{
										var element = '<span id = "' + el_id + '">-</span>';
									}
								}
								break;
							default:
								if(this.editable){
									var element = '<textarea class = "form-control" id = "' + el_id + '" onKeyUp = "basis.update_one(' + params.id + ', \'' + this.name + '\', this.value)">' + this.value + '</textarea>';
								}else{
									var element = '<span id = "' + el_id + '">' + this.value + '</span>';
								}
						}
						$('#' + row_id + '_td_' + this.name).append(
							$(element)
						);

				});
			break;

			default:
				return
		};

		return
	},

	draw_old: function (type, action, params){

		modal.clear();

		/*$(modal.header).append(
			$('<h5></h5>'),
		);*/
		
		$(modal.body).append(
			$('<form id = "basis_' + action + '_form">')
		)

		//UPDATE
		if(action == 'update'){
			params = this.read(params)
			if(typeof(params["error"])!= 'undefined'){
				console.log('error: basis get for update draw: ' + params["error"]["msg"])
				return
			}else if(params["result"]["total"]<1){
				console.log('error: basis get for update draw: not found')
				return
			}
			params = params["result"]["list"][0]
		}

		$.each(this.pattern[type]["draw"][action]["form"], function(item, value){

			if(typeof(params[this["name"]])!= 'undefined') var value = params[this["name"]]
			else if(typeof(this["value"])!= 'undefined') var value = this["value"]
			else var value = ''

			switch(this["type"]){
				case "hidden":
					$('#basis_' + action + '_form').append(
						$('<input type = "hidden" name = "' + this["name"] + '" value = "' + value + '" class = "submit_data" id = "basis_' + action + '_input_' + this["name"] + '" />')
					)
				break

				case "text":
					$('#basis_' + action + '_form').append(
						$('<div class = "form-group">').append(
							$('<label for = "">' + this["title"] + '</label>'),
							$('<input type = "text" name = "' + this["name"] + '" value = "' + value + '" class = "submit_data form-control" id = "basis_' + action + '_input_' + this["name"] + '" />')
						)
					)
				break

				case "textarea":
					$('#basis_' + action + '_form').append(
						$('<div class = "form-group">').append(
							$('<label for = "">' + this["title"] + '</label>'),
							$('<textarea name = "' + this["name"] + '" class = "submit_data form-control" id = "basis_' + action + '_input_' + this["name"] + '" rows = "' + this["rows"] + '">' + value + '</textarea>'),
						)
					)
				break

			}
		})

		$(modal.footer).append(
			$('<div class = "input-group col-7">').append(
				$('<span class = "input-group-text text-danger" id = "basis_' + action + '_msg"></span>'),
				$('<div class = "input-group-append">').append(
					$('<button class = "btn btn-success btn-block" onClick = "basis.submit(\'' + type + '\', \'' + action + '\')">Сохранить</button>'),
					$('<button class = "btn btn-danger" OnClick = "modal.close(\'top\');">Отмена</button>'),
				)
			)
		);

		modal.show('100rem');

		this.editors_draw(type, action)


		/*setTimeout(function() {
			document.getElementById('').focus();
		}, 500);*/

	},

	submit: function (type, action){

		var params = {}
		var error = {
			status: false,
			msg: ''
		}

		$.each(this.pattern[type]["draw"][action]["editors"], function(item, value){
			//this.content.updateSourceElement()
			//var data = CKEDITOR.instances.editor1.getData();
			this.content.updateElement()
		})

		$.each(this.pattern[type]["draw"][action]["form"], function(item, value){

			if(
				typeof(this.required)!== 'undefined' &&
				this.required === true &&
				$('#basis_' + action + '_input_' + this.name).val() == ''
			){
				error.status = true
				error.msg = this.title + '&nbsp;<strong>required</strong>'
			}

			params[this.name] = $('#basis_' + action + '_input_' + this.name).val()

		})

		//console.log(params)
		//console.log(error)

		if(error.status){
			//$('#basis_' + action + '_msg').addClass('text-danger')
			$('#basis_' + action + '_msg').html(error.msg)
			return
		}

		switch(action){

			case "create":
				var answer = this.create(params);
				break

			case "update":
				var answer = this.update(params);
				break

		}

		if(typeof(answer["error"])!= "undefined"){
			$('#basis_' + action + '_msg').html(error.msg)
			return
		}

		location.reload()
	},

	editors_draw: function (type, action){

		$.each(this.pattern[type]["draw"][action]["editors"], function(item, value){

			this.content = CKEDITOR.replace('basis_' + action + '_input_' + this.name);

		})

	},

	//CREATE
	create: function (params){
		wait.start()
		var answer = jsonrpc.request("basis.create", params)
		wait.finish()
		return answer
	},

	//READ
	read: function (params){
		wait.start()
		var answer = jsonrpc.request("basis.read", params)
		wait.finish()
		return answer
	},

	//UPDATE
	update: function (params){

		wait.start()
		var answer = jsonrpc.request("basis.update", params)
		wait.finish()
		return answer

	},
	update_one: function (id, item, val, el_id){

		wait.start()

		var params = {"id": parseInt(id)}

		switch(item){

			case 'order_num':
				params[item] = parseInt(val)
				break
			case 'status':
				if($('#basis_' + id + '_status_btn').hasClass('btn-success')){
					$('#basis_' + id + '_status_btn').removeClass('btn-success').addClass('btn-danger')
					params["closed"] = 1
				}else{
					$('#basis_' + id + '_status_btn').removeClass('btn-danger').addClass('btn-success')
					params["closed"] = 0
				}
				break
			default:
				params[item] = val

		}

		jsonrpc.request("basis.update", params, 1, true, wait.finish)

	},

	//DELETE
	del: function (id, wipe){
		wait.start()
		var params = {
			id: parseInt(id)
		}
		if(typeof(wipe)!== 'undefined' && wipe == 1){
			params.wipe = 1
			$('#basis_' + id).remove();
		}
		var answer = jsonrpc.request("basis.delete", params)
		wait.finish()
		return answer
	},

	//Meta
	update_meta: function (basis_id, name, value){

		wait.start()

		var params = {
			"basis_id": parseInt(basis_id),
			"name": name,
			"value": serialize(value)//serialized string
		}

		var answer = jsonrpc.request("basis.create_meta", params)
		wait.finish()
		return answer

	},

	//Files
	files_create: function (files, type, action, params){

		wait.start()

		if(typeof files == 'undefined') return;

		//data.append('type', type);

		var data = new FormData();
		$.each(files, function(key, value){
			data.append(key, value);
		});

		/*$.each(params, function(key, value){
			data.append(key, value);
		});*/


		$.ajax({
			url			: '/admin/files/create_ajax',
			type		: 'POST',
			data		: data,
			cache		: false,
			dataType	: 'json',
			processData	: false,
			contentType	: false,
			success     : function(respond, status, jqXHR ){
				if(typeof respond.error  ===  'undefined' ){
					//console.log(this);return;
					/*
					mime_type: "image/png"
					​size: 607634
					​size_nice: "593.39 Кб"
					​src: "02-1531-517212210_3.png"
					src_full: "C:\OSPanel\domains\skazka\skazka/files/image/02-1531-517212210_7.png"
					*/
					switch(type){

						/*case 'banner_top':
							switch(action){

								case 'create':
								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									//params["img"] = this.src

									var answer = jsonrpc.request("img.resize", {
										"input": {//require, array
											"file": this.src_full //require, string, 2-255 path to file.
										},
										"base_size": {//array
											"sizes": ['big'], //array 'all' / 'none' / 'thumbnail','medium','big'
											"centered": 0, //1 / 0,
											"save": 1, // 1 / 0
										},
									});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									//console.log(answer);return;

									params["img"] = answer["result"]["base_size"]["big"]["name"];
									var answer = jsonrpc.request("basis.create", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									document.location.reload(true);
									
									answer = jsonrpc.request("basis.read", {id: answer["result"]["id"]});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									if(answer["result"]["total"]<1){
										console.log('not found')
										return true
									}
									var result = answer["result"]["list"][0];

									//console.log(result)

									basis.draw_table_row(type, 'init', result)
									
								});

								break
							}

						break*/

						/*case 'news':
							switch(action){

								case 'create':

								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									params["img"] = this.src

									var answer = jsonrpc.request("basis.create", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									//
									document.location.reload(true);
									return true;
									//
									answer = jsonrpc.request("basis.read", {id: answer["result"]["id"]});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									if(answer["result"]["total"]<1){
										console.log('not found')
										return true
									}
									var result = answer["result"]["list"][0];

									//console.log(result)

									basis.draw_table_row(type, 'init', result)

								});
								break
							}
						break*/

						case 'file_product':
							switch(action){

								case 'create':

								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									params["img"] = this.src

									var answer = jsonrpc.request("basis.create", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									answer = jsonrpc.request("basis.read", {id: answer["result"]["id"]});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									if(answer["result"]["total"]<1){
										console.log('not found')
										return true
									}
									var result = answer["result"]["list"][0];

									//console.log(result)

									basis.draw_table_row(type, 'init', result)

								});

								break
							}
						break;

						default:
							switch(action){
								case 'create':
								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									//params["img"] = this.src

									var answer = jsonrpc.request("img.resize", {
										"input": {//require, array
											"file": this.src_full //require, string, 2-255 path to file.
										},
										"base_size": {//array
											"sizes": ['big'], //array 'all' / 'none' / 'thumbnail','medium','big'
											"centered": 0, //1 / 0,
											"save": 1, // 1 / 0
										},
									});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									//console.log(answer);return;

									params["img"] = answer["result"]["base_size"]["big"]["name"];
									var answer = jsonrpc.request("basis.create", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									document.location.reload(true);
									
									/*answer = jsonrpc.request("basis.read", {id: answer["result"]["id"]});
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}
									if(answer["result"]["total"]<1){
										console.log('not found')
										return true
									}
									var result = answer["result"]["list"][0];

									//console.log(result)

									basis.draw_table_row(type, 'init', result)*/
									
								});

								break
							}

						break;
					}

					wait.finish()

				}else console.log('AJAX ERROR: ' + respond.error);
			},
			error: function(jqXHR, status, errorThrown){
				console.log('AJAX REQUEST ERROR: ' + status, jqXHR);
			}
		});
	},

	files_update: function (files, type, action, params){

		wait.start()

		if(typeof files == 'undefined') return;

		//data.append('type', type);

		var data = new FormData();
		$.each(files, function(key, value){
			data.append(key, value);
		});

		$.ajax({
			url			: '/admin/files/create_ajax',
			type		: 'POST',
			data		: data,
			cache		: false,
			dataType	: 'json',
			processData	: false,
			contentType	: false,
			success     : function(respond, status, jqXHR ){
				if(typeof respond.error  ===  'undefined' ){                                         

					switch(type){

						case 'file_menu':
						case 'file_barmap':
							switch(action){
								case 'update':
								$.each(respond.list, function(key, value){
									var answer = basis.update_meta(params.id, type, this.src);
									if(typeof(answer["error"]) != 'undefined'){
										console.log(answer["error"]["msg"])
										$('#basis_' + params.id + '_' + type + '_desc').html(answer["error"]["msg"])
										return true
									}

									$('#basis_' + params.id + '_' + type + '_desc').html(this.src + '<br /><strong>Загружено ' + this.size_nice + '</strong>')
								})
								break
							}
						break;

						/*case 'file_product':

							switch(action){

								case 'update':
								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									params["img"] = this.src

									var answer = jsonrpc.request("basis.update", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									$('#basis_' + params.id + '_img').attr("src", '/files/image/' + params.img)

								})
								break
							}

						break;*/

						default:
							switch(action){

								case 'update':
								$.each(respond.list, function(key, value){
									params["mime_type"] = this.mime_type
									params["img"] = this.src

									var answer = jsonrpc.request("basis.update", params);
									if(typeof(answer["error"])!= 'undefined'){
										console.log(answer["error"]["msg"])
										return true
									}

									$('#basis_' + params.id + '_img').attr("src", '/files/image/' + params.img)

								})
								break

							}
					}

					wait.finish()

				}else console.log('AJAX ERROR: ' + respond.error);
			},
			error: function(jqXHR, status, errorThrown){
				console.log('AJAX REQUEST ERROR: ' + status, jqXHR);
			}
		});
	},

	youtube_create_draw: function (id_parent){

		modal.clear();

		$(modal.header).append(
			$('<h5>Добавление видео</h5>'),
		)
		
		$(modal.body).append(
			$('<form id = "basis_youtube_create_form">').append(
				$('<input type = "hidden" name = "id_parent" value = "' + id_parent + '" />'),
				$('<div class = "form-group">').append(
					$('<label for = "basis_youtube_create_id">youtube номер<br /><span class = "small text-muted">https://www.youtube.com/watch?v = </span><strong>RKfQ7EalCbI</strong>&emsp;</label>'),
					$('<input type = "text" name = "img" value = "" form-control" id = "basis_youtube_create_id" />')
				)
			),
		)

		$(modal.footer).append(
			$('<div class = "input-group col-7">').append(
				$('<div class = "input-group-append">').append(
					$('<button class = "btn btn-success btn-block" onClick = "basis.youtube_create_submit(' + id_parent + ')">Сохранить</button>'),
					$('<button class = "btn btn-danger" OnClick = "modal.close(\'top\');">Отмена</button>'),
				)
			)
		)

		modal.show('50rem');

		setTimeout(function() {
			document.getElementById('basis_youtube_create_id').focus();
		}, 300)
	},

	youtube_create_submit: function (id_parent){

		if($('#basis_youtube_create_id').val() == '') return

		var params = {
			id_parent: id_parent,
			type: 'file_product',
			mime_type: 'video/youtube_id',
			img: $('#basis_youtube_create_id').val()
		}

		//console.log(params)

		var answer = this.create(params);
		if(typeof(answer["error"])!= "undefined"){
			console.log(error.msg)
			return
		}

		location.reload()
	},


}

