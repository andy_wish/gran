var person = {

	draw: function(data, target){

		target.append(
			$('<tr class="active_mark" id="person_' + data.id + '">').append(
/*				$('<td style="width:2rem" class="text-right"><i id="person_' + data.id + '_id">' + data.id + '</i></td>'),
				$('<td style="width:2rem"><img src="/files/image/' + data.img + '" id="person_' + data.id + '_img" title="/files/image/' + data.img + '" width="20" height="20" />'),*/
				$('<td  style="width:18rem"><a href="/person/read/' + data.id + '"><i class="far fa-address-card"></i></a>&nbsp;<strong>' + data.surname + '</strong> ' + data.name + ' ' + data.patronymic + '</td>'),
				$('<td style="width:20rem"><input type="text" class="form-control form-control-sm py-0" id="person_' + data.id + '_phone" value="' + data.phone + '" onKeyUp="person.update_one(' + data.id + ', \'phone\', this.value)" /></td>'),
				$('<td><input type="text" class="form-control form-control-sm py-0" id="person_' + data.id + '_address" value="' + data.address + '" onKeyUp="person.update_one(' + data.id + ', \'address\', this.value)" /></td>'),
				$('<td><input type="text" class="form-control form-control-sm py-0" id="person_' + data.id + '_description" value="' + data.description + '" onKeyUp="person.update_one(' + data.id + ', \'description\', this.value)" /></td>'),
				$('<td><input type="text" class="form-control form-control-sm py-0" id="person_' + data.id + '_radio" value="' + data.radio + '" onKeyUp="person.update_one(' + data.id + ', \'radio\', this.value)" /></td>'),
				$('<td style="max-width:9rem;word-wrap:break-word">' + data.place_name + '<br />' + data.position_name + '</td>'),
			),
		);
	},

	create: function (params){
		wait.start()
		var answer = jsonrpc.request("person.create", params)
		wait.finish()
		return answer
	},

	read: function (params){
		wait.start()
		var answer = jsonrpc.request("person.read", params)
		wait.finish()
		return answer
	},

	update: function (params){
		wait.start()
		var answer = jsonrpc.request("person.update", params)
		wait.finish()
		return answer
	},

	update_one: function (id, item, val, el_id){
		wait.start()
		var params = {"id": parseInt(id)}

		switch(item){

			case 'order_num':
				params[item] = parseInt(val)
				break
			case 'status':
				if($('#person_' + id + '_status_btn').hasClass('btn-success')){
					$('#person_' + id + '_status_btn').removeClass('btn-success').addClass('btn-danger')
					params["closed"] = 1
				}else{
					$('#person_' + id + '_status_btn').removeClass('btn-danger').addClass('btn-success')
					params["closed"] = 0
				}
				break
			default:
				params[item] = val

		}

		jsonrpc.request("person.update", params, 1, true, wait.finish)
	},
	
	update_radio: function (person_id_to, radio_name){//

		var answer = jsonrpc.request("person.read", {radio: radio_name});
		if(answer.result.total > 0){
			var person_id_from = answer.result.list[0]["id"]
			jsonrpc.request("person.update", {id: parseInt(person_id_from), "radio": ''}, 1, true);
		}

		if(person_id_to > 0) jsonrpc.request("person.update", {id: person_id_to, "radio": radio_name}, 1, true, wait.finish);

	},
	
	//person.update_one(this.value, 'radio', '<?=$row["name"]?>')
	

	del: function (id, wipe){
		wait.start()
		var params = {
			id: parseInt(id)
		}
		if(typeof(wipe)!== 'undefined' && wipe == 1){
			params.wipe = 1
			$('#person_' + id).remove();
		}
		var answer = jsonrpc.request("person.delete", params)
		wait.finish()
		return answer
	},

	total : 0,
	exists : 0,
	added : 0,
	updated : 0,


}