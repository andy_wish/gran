var ubnt = {

	//CREATE
	create: function (params){
		wait.start()
		var answer = jsonrpc.request("ubnt.create", params)
		wait.finish()
		return answer
	},

	//READ
	read: function (params){
		wait.start()
		var answer = jsonrpc.request("ubnt.read", params)
		wait.finish()
		return answer
	},

	reboot_one: function (ip){

		var ip_norm = ip.replace(/\./g, "_");
		$('#ubnt-' + ip_norm + '-reboot').attr('disabled', 'disabled');
		//ubnt.draw_offline($('#ubnt-' + ip_norm + '-online'));
		setTimeout(function(){
			$('#ubnt-' + ip_norm + '-reboot').removeAttr('disabled');
		}, 45000);

		jsonrpc.request("ubnt.command", {ip: ip, command: 'reboot'}, 1, true, ubnt.reboot_one_result);
		setTimeout(function(){
			jsonrpc.request("ubnt.command", {ip: ip, command: 'reboot'}, 2, true, ubnt.reboot_one_result);
		}, 1000);
		setTimeout(function(){
			jsonrpc.request("ubnt.command", {ip: ip, command: 'reboot'}, 3, true, ubnt.reboot_one_result);
		}, 5000);

	},

	reboot_one_result: function (answer){

		if(typeof(answer["error"])!= "undefined"){
			console.log(answer.error.msg);
			$('#ubnt_info_msg').html(answer.error.msg);
			return;
		}

		var ip_norm = answer.result.ip.replace(/\./g, "_");
		ubnt.draw_offline($('#ubnt-' + ip_norm + '-online'));
		setTimeout(function(){
			ubnt.search_one(answer.result.ip);
		}, 45000);


	},

	reboot_list: function(){
		var checked_list = ubnt.get_checked_list();
		var reboot_list = {};

		$.each(checked_list, function(item, value){
			if(typeof(reboot_list[this.reboot_priority]) == 'undefined') reboot_list[this.reboot_priority] = [];
			reboot_list[this.reboot_priority].push(this.ip);
		});

		//console.log(reboot_list);
		$.each(reboot_list, function(item, value){
			//console.log('item: '+item);
			$.each(this, function(it, val){
				console.log('rebooting: ' + val + ' fromlist');
				ubnt.reboot_one(val);
			});
		});
		return;
	},

	get_checked_list: function(){
		var list = [];
		$.each($('.ubnt-checkbox'), function(item, value){
			if($(this).prop('checked')){
				var ip_norm = $(this).data('ip_norm');
				var data = {
					id: $(this).data('id'),
					ip_norm: ip_norm,
					ip: $(this).data('ip'),
					name: $(this).data('name'),
					model_name: $(this).data('model_name'),
					reboot_priority: $('#ubnt-' + ip_norm + '-reboot_priority').val(),
				}
				list.push(data)
				//console.log(data);
			}
		});
		//console.log(list);
		return list;
	},

	all_checkbox_check: function(){
		if($('#ubnt-all-checkbox').prop('checked')) var checked = true;
		else var checked = false;
		$.each($('.ubnt-checkbox'), function(item, value){
			$(this).prop('checked', checked);
		});
	},

	command: function (ip, command){
		wait.start()
		var answer = jsonrpc.request("ubnt.command", {ip: ip, command: command});
		if(typeof(answer["error"])!= "undefined"){
			console.log(answer.error.msg)
			wait.finish()
			return error.msg
		}
		wait.finish()
		return answer["result"];
	},

	search: function (){

		$('#ubnt_btn_search').attr('disabled', 'disabled');
		$('.ubnt-online').attr('disabled', 'disabled');
		ubnt.draw_offline($('.ubnt-online'));
		$('#ubnt_btn_search').removeClass('btn-primary').addClass('btn-outline-primary');
		$('#ubnt_btn_search').html('идёт поиск..');
		setTimeout(function(){
			$('#ubnt_btn_search').removeAttr('disabled');
			$('.ubnt-online').removeAttr('disabled');
			$('#ubnt_btn_search').removeClass('btn-outline-primary').addClass('btn-primary');
			$('#ubnt_btn_search').html('поиск');
		}, 35000);

		jsonrpc.request("ubnt.search", {timeout: 1}, 1, true, ubnt.search_result);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {timeout: 3}, 2, true, ubnt.search_result);
		}, 1500);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {timeout: 7}, 3, true, ubnt.search_result);
		}, 5000);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {timeout: 20}, 4, true, ubnt.search_result);
		}, 12500);

	},

	search_one: function (ip_search){

		var ip_norm = ip_search.replace(/\./g, "_");
		$('#ubnt-' + ip_norm + '-online').attr('disabled', 'disabled');
		ubnt.draw_offline($('#ubnt-' + ip_norm + '-online'));
		setTimeout(function(){
			$('#ubnt-' + ip_norm + '-online').removeAttr('disabled');
		}, 35000);

		jsonrpc.request("ubnt.search", {ip: ip_search, timeout: 1}, 1, true, ubnt.search_one_result);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {ip: ip_search, timeout: 3}, 2, true, ubnt.search_one_result);
		}, 1500);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {ip: ip_search, timeout: 7}, 3, true, ubnt.search_one_result);
		}, 5000);
		setTimeout(function(){
			jsonrpc.request("ubnt.search", {ip: ip_search, timeout: 20}, 4, true, ubnt.search_one_result);
		}, 12500);
	},

	search_result: function (answer){

		if(typeof(answer["error"])!= "undefined"){
			console.log(answer.error.msg);
			$('#ubnt_info_msg').html(answer.error.msg);
			return;
		}

		if(answer.result.total > 0){
			$.each(answer.result.list, function(item, value){
				var ip_norm = value.ip.replace(/\./g, "_");
				//console.log(value.ip + ' is online');
				ubnt.draw_online($('#ubnt-' + ip_norm + '-online'));
			});
		}

	},

	search_one_result: function (answer){

		if(typeof(answer["error"])!= "undefined"){
			console.log(answer.error.msg);
			$('#ubnt_info_msg').html(answer.error.msg);
			return;
		}

		if(answer.result.total > 0){
			var ip_norm = answer.result.list[0].ip.replace(/\./g, "_");
			//console.log(answer.result.list[0].ip + ' is online');
			ubnt.draw_online($('#ubnt-' + ip_norm + '-online'));
		}else{
			//ubnt.draw_offline($('#ubnt-' + ip_norm + '-online'));
		}

	},

	draw_online: function (el) {
		el.removeClass('btn-light').addClass('btn-success');
	},
	draw_offline: function (el) {
		el.removeClass('btn-success').addClass('btn-light');
	},

	update_one: function (id, item, val, el_id){

		wait.start()

		var params = {"id": parseInt(id)}

		switch(item){

			case 'reboot_priority':
				params[item] = parseInt(val);
				break;
			default:
				params[item] = val;

		}

		jsonrpc.request("hardware.update", params, 1, true, wait.finish)

	},

}

