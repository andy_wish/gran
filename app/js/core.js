﻿var jsonrpcUrl='http://gran.gt/jsonrpc/';
//var jsonrpcUrl='http://gran/jsonrpc/';

var jsonrpc, modal, wait;
var search;
var person;

function Wait(){

	$('body').prepend(
		$('<div id="wait_notify" style="position:fixed;top:90%;left:90%;z-index:9999;opacity:.5;display:none" />').append(
			$('<i class="fas fa-cog fa-5x fa-spin" />')
		)
	);

	this.start = function(){
		$('#wait_notify').fadeIn();
	}

	this.finish = function(num, msg){
		$('#wait_notify').fadeOut();
	}

}

function Modal(tag) {

	"use strict"

	this.tag = tag.replace(/['"«»]/g, '');

	if($('#modal_'+this.tag).length){
		console.log('modal_'+this.tag+' already exist')
		return
	}

	$('body').prepend(
		$('<div class="modal fade" id="modal_'+this.tag+'" tabindex="-1" role="dialog" aria-hidden="true" />').append(
			$('<div class="modal-dialog modal_box" id="modal_window_'+this.tag+'" role="document" />').append(
				$('<div class="modal-content" id="modal_box_'+this.tag+'" />').append(
					$('<button class="btn btn-sm btn-secondary m-1" style="position:absolute;right:0;" onClick="modal.close(\''+this.tag+'\')" aria-label="Close" />').append(
						$('<i class="fas fa-times" />')
					),
					$('<div class="modal-header pr-5" id="modal_header_'+this.tag+'" />'),
					$('<div class="modal-body" id="modal_body_'+this.tag+'" />'),
					$('<div class="modal-footer" id="modal_footer_'+this.tag+'" />')
				)
			)
		)
	);

	this.header = '#modal_header_'+this.tag;
	this.body = '#modal_body_'+this.tag;
	this.footer = '#modal_footer_'+this.tag;

	this.show = function(width){
		var width = width === undefined ? '60rem' : width;
		$('#modal_window_'+this.tag).css('max-width', width);
		$('#modal_'+this.tag).modal();
	};

	this.clear = function(){
		$(this.header).empty();
		$(this.body).empty();
		$(this.footer).empty();
	};

	this.close = function(tag){
		console.log(tag)
		$('#modal_'+tag).modal('hide');
	};

}

var cookie = {

	set: function(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";},

	get: function(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}

}


$(document).ready(function(){

	jsonrpc = new Jsonrpc(jsonrpcUrl, cookie.get('data'), cookie.get('lang'));

	modal = new Modal('top');

	wait = new Wait();

	//SEARCH
	search = new Search('person-search', 'person-list', 'person-count');
	$('#person-search').keyup(function(e){
		search.key(e.keyCode)
	});

});

function serialize(mixed_val) {
    // Generates a storable representation of a value
    // +   original by: Ates Goral (http://magnetiq.com)
    // +   adapted for IE: Ilia Kantor (http://javascript.ru)
 
    switch (typeof(mixed_val)){
        case "number":
            if (isNaN(mixed_val) || !isFinite(mixed_val)){
                return false;
            } else{
                return (Math.floor(mixed_val) == mixed_val ? "i" : "d") + ":" + mixed_val + ";";
            }
        case "string":
            //return "s:" + mixed_val.length + ":\"" + mixed_val + "\";";
			return "s:" + get_length(mixed_val) + ":\"" + mixed_val + "\";";
        case "boolean":
            return "b:" + (mixed_val ? "1" : "0") + ";";
        case "object":
            if (mixed_val == null) {
                return "N;";
            } else if (mixed_val instanceof Array) {
                var idxobj = { idx: -1 };
		var map = []
		for(var i=0; i<mixed_val.length;i++) {
			idxobj.idx++;
                        var ser = serialize(mixed_val[i]);
 
			if (ser) {
                        	map.push(serialize(idxobj.idx) + ser)
			}
		}                                       

                return "a:" + mixed_val.length + ":{" + map.join("") + "}"

            }
            else {
                var class_name = get_class(mixed_val);
 
                if (class_name == undefined){
                    return false;
                }
 
                var props = new Array();
                for (var prop in mixed_val) {
                    var ser = serialize(mixed_val[prop]);
 
                    if (ser) {
                        props.push(serialize(prop) + ser);
                    }
                }
                return "O:" + class_name.length + ":\"" + class_name + "\":" + props.length + ":{" + props.join("") + "}";
            }
        case "undefined":
            return "N;";
    }
 
    return false;
}
function get_length(val){
	var counter = 0;
	for(i=0; val.length>i; i++)
		if( /[абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ]/.test(val[i]) )
			counter += 2;
		else counter++;
	return counter;
}
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}
