<?php
define('DEBUG', true);
define('INBOX', 1);

define('ROOT_DIR', dirname(__FILE__));
define('LIB_DIR', ROOT_DIR.'/lib');
define('FILES_DIR', ROOT_DIR.'/files');
define('IMG_DIR', FILES_DIR.'/image');

define('ROOT_WEBDIR', '//'.$_SERVER['HTTP_HOST']);
define('FILES_WEBDIR', ROOT_WEBDIR.'/files');
define('IMG_WEBDIR', FILES_WEBDIR.'/image');

require 'config.php';

if(DEBUG){
	ini_set('display_errors',1);
	error_reporting(E_ALL);
}

spl_autoload_register(function($class) {
	require(LIB_DIR.'/'.strtolower($class).'.php');
});

$routes=explode('/', $_SERVER['REQUEST_URI']);
switch($routes[1]){
	case 'jsonrpc':
		require ROOT_DIR.'/jsonrpc/boot.php';
		break;
	case 'run':
		require ROOT_DIR.'/run/boot.php';
		break;
	/*case 'admin':
		require ROOT_DIR.'/admin/boot.php';
		break;*/
	default:
		require ROOT_DIR.'/app/boot.php';
}