<?php if (!defined("INBOX")) die('separate call');

Cache::init();

class Cache {

	protected static $_instance;
	protected static $config;
	protected static $path = ROOT_DIR.'/cache/';
	protected static $ext.='.cache';
	//protected static $file_name;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init() {
		if (self::$_instance === null) {
			self::$_instance = new self;
			self::$config=[];
			if (!is_dir($path)) mkdir($path, 0777);
		}
	}

	public static function check($name){//
		if self::read($name)
		ob_start();
	}

	private static function read($name){//
		if (file_exists($path.$name)) {
		/*if ($compres) {
			header('content-encoding: gzip');
			header('vary: accept-encoding');
			header('content-length: ' . strlen($html));
		}*/
		return file_get_contents($path.$name);
		}else return false;
	}
	
	public static function finish(){//
		if(self::$name){
			$html = Optimize::html(ob_get_contents());
			if ($compres) $html = gzencode($html);
			ob_end_flush();
			$fp = fopen(self::$name, 'w');
			fwrite($fp, $html);
			fclose($fp);
		}
	}
	

}