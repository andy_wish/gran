<?php if (!defined("INBOX")) die('separate call');

class Ubnt {


	public static function command($params){

		$params = Verify::filter($params, [
			"command" => [
				"type" => 'string',
				"min" => 1,
				"max" => 255,
				"require" => true,
			],
			"id"=>[
				"type" => 'int',
				"min" => 1
			],
			"ip"=>[
				"type"=>'string',
				"min"=> 7,
				"max"=> 15
			],
			"login" => [
				"type" => 'string',
				"min" => 3,
				"max" => 255
			],
			"password" => [
				"type" => 'string',
				"min" => 3,
				"max" => 255
			],
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["ip"]) AND !isset($params["id"])) return ["error" => 'ip or id required'];
		if(!isset($params["login"])){
			if(User::group_id()==9) $params["login"] = Core::config('ubnt_login');
			else return ["error" => 'login required'];
		};
		if(!isset($params["password"])){
			if(User::group_id()==9) $params["password"] = Core::config('ubnt_password');
			else return ["error" => 'password required'];
		};

		if(!isset($params["ip"]) AND (!$params["ip"] = Hardware::get_ip_by_id($params["id"]))) return ["error" => 'ip by id not found'];

		$answer = self::command_send( $params["ip"], $params["command"], $params["login"], $params["password"] );
		if(isset($answer["error"])) return $answer;

		return ["ip" => $params["ip"], "answer" => $answer];
	}

	private static function command_send( $ip, $command, $login, $password ) {

		//reboot alias bg break cd chdir command continue echo eval exec exit export false fg getopts hash help history jobs kill let local printf pwd read readonly return set shift source test times trap true type ulimit umask unalias unset wait

		if (!$c = @ssh2_connect($ip, 22)) return ["error" => $ip.' connect error'];
		if (!ssh2_auth_password($c, $login, $password)) return ["error" => $ip.' auth error'];
		$stream = ssh2_exec($c, $command);
		stream_set_blocking($stream, true);
		$stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
		$answer = stream_get_contents($stream_out);
		fclose($stream);
		return $answer;
	}

	public static function search($params){

		$params = Verify::filter($params, [
			"timeout" => [
				"type" => 'int',
				"min" => 0,
				"max" => 255
			],
			"ip"=>[
				"type"=>'string',
				"min"=> 7,
				"max"=> 15
			],
			"save_hardware_results" => [
				"type" => 'int',
				"min" => 0,
				"max" => 1
			],
			"save_online_results" => [
				"type" => 'int',
				"min" => 0,
				"max" => 1
			],
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["ip"])) $params["ip"] = '255.255.255.255';
		if(!isset($params["timeout"])) $params["timeout"] = 7;
		if(!isset($params["save_hardware_results"])) $params["save_hardware_results"] = 0;
		if(!isset($params["save_online_results"])) $params["save_online_results"] = 0;

		$result = [
			"total" => 0,
			"list" => [],
			"broadcast_ip" => $params["ip"],
			"timeout" => $params["timeout"]
		];

		$data_send = chr(1).chr(0).chr(0).chr(0);

		$socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		socket_set_option($socket, SOL_SOCKET, SO_BROADCAST, 1); 
		socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ["sec" => $params["timeout"], "usec" => 0]);

		if(!socket_sendto($socket, $data_send, 4, 0, $params["ip"], 10001)) {
			return ["error" => 'socket_sendto: ' . socket_last_error() .', msg: ' . iconv('WINDOWS-1251', 'UTF-8', socket_strerror(socket_last_error()))];
		}

		while(true) {
			$ret = @socket_recvfrom($socket, $buf, 200, 0, $ip_hardware, $port_hardware);
			if($ret === false) break;
			$result["total"]++;
			$result["list"][] = [
				"ip"=> $ip_hardware,
				"port" => $port_hardware,
				"answer" => utf8_encode($buf)
			];
		}
		socket_close($socket);

		if($params["save_hardware_results"]) self::search_hardware_save($result);
		if($params["save_online_results"]) self::search_online_save($result);

		return $result;
	}

	private static function search_hardware_save($result){//переписать с нормальным разбором строки и с безопасной вставкой в дб

		if($result["total"] < 1) return true;

		$models = DB::getAll('SELECT * FROM `model`');

		$q = 'INSERT IGNORE INTO `hardware` (`type`, `name`, `model_id`, `ip`, `version`, `description`) VALUES ';

		foreach($result["list"] as $row){
			//? hrQj?-??hrQj?- ?? NS_Bashnya APN2N Gran_AP"XM.ar7240.v6.3.0.33226.200428.1849?NanoStation M2
			//echo $row["answer"].'<br />';

			$model_id = 0;
			foreach($models as $model){
				if($pos = stripos($row["answer"], $model["name"])){
					$model_id = $model["id"];
					break;
				}
			}
			/*$data = explode('"', $row["answer"], 2);
			$info = explode('', $data[1]);
			$version = trim($info[0]);
			$data = explode('', $data[0]);
			$host_name = trim($data[0]);//?*/

			/*echo 'host_name: '.$host_name.'<br />';
			echo 'model: '.$model_id.'<br />';
			echo 'ver: '.$version.'<br />';*/
			
			$version = 'n/a';
			$host_name = 'n/a';

			$q .= DB::parse('(1, ?s, ?i, ?s, ?s, ?s), ', $host_name, $model_id, $row["ip"], $version, $row["answer"]);

		}

		$q = rtrim($q, ', ');
		$q .=';';
		//die($q);
		DB::query($q);

	}

	private static function search_online_save($result){//переписать с нормальным разбором строки и с безопасной вставкой в дб

		if($result["total"] < 1) return true;

		$hardware_online = $result["list"];

		$hardware = Hardware::read([
			"type" => 1,
			"response" => [
				"order" => 'ip',
			]
		]);
		$hardware_db = [];
		foreach ($hardware["list"] as $row){
			$hardware_db[$row["ip"]] = $row["id"];
		}
		

//var_dump($hardware_db);

		$q = 'INSERT INTO `online` (`hardware_id`, `status`) VALUES ';

		foreach($hardware_online as $row){
			//echo 'in array? '.$row["ip"].'<br />';
			if(array_key_exists($row["ip"], $hardware_db)){
				//echo 'in array '.$row["ip"].'('.$hardware_db[$row["ip"]].')<br />';
				$q .= '('.$hardware_db[$row["ip"]].', 1), ';
				unset($hardware_db[$row["ip"]]);
			}
			//echo '<hr />';
		}
		foreach($hardware_db as $name => $value){
			$q .= '('.$value.', 0), ';
		}
		$q = rtrim($q, ', ');
		
		
		//var_dump($hardware_online);
		
//die($q);
		DB::query($q);

	}


	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	protected const DATA_DESC=[//?
			"id"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>18446744073709551615
			],
			"type"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>65365
			],
			"created"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"command" => [
				"type" => 'string',
				"min" => 1,
				"max" => 255
			],
			"model_id"=>[
				"type" => 'int',
				"min" => 1
			],
			"description" => [
				"type" => 'string',
				"min" => 0,
				"max" => 255
			],
			"reboot_priority"=>[
				"type" => 'int',
				"min" => 0,
				"max" => 255
			],

			"closed"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>2
			],
			"response"=>[
				"type"=>'helper'
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>255
			]
		];

}