<?php if (!defined("INBOX")) die('separate call');

Text::init();

class Text{

	protected static $_instance;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init(){
		if (self::$_instance === null) {
			self::$_instance = new self();
		}
	}

	public static function translit($s) {
		//$s=(string) $s; // в строковое значение
		//$s=strip_tags($s); // HTML-теги
		$s=str_replace(array("\n", "\r"), " ", $s); // перевод каретки
		$s=str_replace("+", " ", $s);
		$s=str_replace("/", " ", $s);
		$s=str_replace(".", " ", $s);
		$s=trim($s); // пробелы в начале и конце строки
		$s=strtr($s, array(
			'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'',
			'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z','И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'','Ь'=>''
		));
		$s=preg_replace("/[^0-9a-z-_ ]/i", "", $s); // от недопустимых символов
		$s=preg_replace("/\s+/", ' ', $s); // повторяющие пробелы
		$s=str_replace(" ", "-", $s); // пробелы знаком минус
		return $s;
	}

	public function textswitch($text, $mode='FROM_RU'){
		$str_search = array(
			"й", "ц", "у", "к", "е", "н", "г", "ш", "щ", "з", "х", "ъ",
			"ф", "ы", "в", "а", "п", "р", "о", "л", "д", "ж", "э",
			"я", "ч", "с", "м", "и", "т", "ь", "б", "ю"
		);
		$str_replace = array(
			"q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]",
			"a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "",
			"z", "x", "c", "v", "b", "n", "m", ",", "."
		);
		if ($mode == 'FROM_RU')
			return str_replace($str_search, $str_replace, $text);
		if ($mode == 'FROM_EN')
			return str_replace($str_replace, $str_search, $text);
	}

	public static function random_string($l=32){
		$s=str_shuffle('abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
		return substr($s, 1, $l);
	}

	/*function strToHex($string){
		$hex='';
		for ($i=0; $i < strlen($string); $i++){
			$hex .= dechex(ord($string[$i]));
		}
		return $hex;
	}
	function str_to_hex_array($str){
		$arr = [];
		for ($i=0; $i < strlen($str); $i++){
			$arr[] = dechex(ord($str[$i]));
		}
		return $arr;
	}*/

}