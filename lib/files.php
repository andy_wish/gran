<?php if (!defined("INBOX")) die('separate call');

Files::init();

class Files {

	protected static $_instance;
	protected static $config;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init() {
		if (self::$_instance === null) {
			self::$_instance = new self;
			self::$config=[
				"max_file_size"=> 52428800, //50Mb
				"types_allowed"=> [
					"banner"=>[
						"jpeg"=> 'image',
						"png"=> 'image',
						"gif"=> 'image'
					],
					"product"=>[
						"jpeg"=> 'image',
						"png"=> 'image',
						"gif"=> 'image',
						"pdf"=> 'pdf',
						"msword"=> 'word',
						"mspowerpoint"=> 'powerpoint',
						"vnd.ms-powerpoint"=> 'powerpoint',
						"x-zip-compressed"=> 'archive'
					],
				],
				"upload_accept"=>[
					"banner"=>'image/jpeg, image/png, image/gif',
					"product"=>'image/jpeg, image/png, image/gif, application/pdf, application/msword, application/zip, application/mspowerpoint, application/vnd.ms-powerpoint',
				]
			];
		}
	}

	public static function new_name($path, $name){
		$name=strtr($name, ' ', '_');
		$p=pathinfo($name);
		$ext=$p['extension'];
		$basename=rtrim($p['basename'], '.'.$ext);
		$i=1;
		while(true){
			if(!file_exists($path.$name)) break;
			$i++;
			$name=$basename.'_'.$i.'.'.$ext;
		}
		return $name;
    }

	public static function size_convert($n){
		if($n<1024) return $n.' Байт';
		elseif($n<1048576) {
			$n=round($n/1024, 2);
			return $n.' Кб';
		}else{
			$n=round($n/1048576, 2);
			return $n.' Мб';
		}
		return $n;
	}

	public static function upload($file){

			if($file['error']!=0) return ["error"=> 'transmit error ['.$file['error'].']'];
			if(!isset($file['name'])) return ["error"=> 'name required'];
			if(!isset($file['type'])) return ["error"=> 'file type required'];
			if(!isset($file['tmp_name'])) return ["error"=> 'tmp_name required'];
			if(!isset($file['size'])) return ["error"=> 'size required'];

			if($file['size']>Core::config('file_upload_max_size')) return ["error"=> 'max file size is '.Core::config('file_upload_max_size').' ['.$file['size'].']'];




					/*Core::config('banner_upload_allowed') //image/jpeg, image/png, image/gif
					, self::$config["types_allowed"]["$target"]["$type"]))  return array("error"=> 'file type not supported ['.$type.']');
				Добавить проверку на допустимые типы файлов*/


			$params["mime_type"]=$file['type'];
			$type=explode('/', $file['type']);
			$type=$type[0];
			$path=ROOT_DIR.'/files/'.$type.'/';
			if(!is_dir($path)) mkdir($path);

			$params["src"]=self::new_name($path, $file['name']);
			$dest=$path.$params["src"];

			if(move_uploaded_file($file['tmp_name'], $dest)){
				$result["src"]=$params["src"];
				$result["mime_type"]=$params["mime_type"];
				$result["size"]=$file['size'];
				$result["size_nice"]=self::size_convert($file['size']);
			}else{
				$result["error"]='cant move_uploaded_file ['.$dest.']';
			}
			
			return $result;
	}

	public static function upload_accept_type($mode){//
		if(isset(self::$config["upload_accept"]["$mode"])) return self::$config["upload_accept"]["$mode"];
		else return '';
	}

}