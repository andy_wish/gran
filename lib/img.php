<?php if (!defined("INBOX")) die('separate call');

Img::init();

class Img{

	private static $_instance;
	private static $sizes=[];

	protected static $config=[];

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){
		self::$sizes = explode(',', Core::config('img_sizes'));//thumbnail,medium,big
		foreach(self::$sizes as $size_name){
			self::$config["size"]["$size_name"]["width"]=Core::config('img_'.$size_name.'_size_w');
			self::$config["size"]["$size_name"]["height"]=Core::config('img_'.$size_name.'_size_h');
		}
	}

	public static function init() {
		if (self::$_instance === null) {
			self::$_instance = new self();
		}
	}

	private static function load(array $params){
		$info=getimagesize($params["file"]);
		if(!$info) return ["error"=>'getimagesize error'];

		$params["width"]=$info[0];
		$params["height"]=$info[1];
		$params["ext"]=$info[2];
		$params["mime"]=$info["mime"];

		switch($params["ext"]){
			case 2:
				$params["resource"]=imagecreatefromjpeg($params["file"]);
				$params["ext"]='jpg';
				break;
			case 1:
				$params["resource"]=imagecreatefromgif($params["file"]);
				$params["ext"]='gif';
				break;
			case 3:
				$params["resource"]=imagecreatefrompng($params["file"]);
				$params["ext"]='png';
				break;
			default:
				return ["error"=>'mime type not supported. jpg, png, gif only'];
		}
		return $params;
	}

	public static function resize(array $params){

		/*new_size=> [
			"height"=>100,
			"width"=>100,
		],
		base_size=> [
			'all' / 'none' / 'thumbnail','medium','big'
		]*/

		$params=Verify::filter($params, [
			"input"=>[
				"require"=>true,
				"type"=>'array',
			],
			"new_size"=>[
				"type"=>'array',
			],
			"base_size"=>[
				"type"=>'array',
			],
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["base_size"]) AND !isset($params["new_size"])) return ["error"=> 'nothing to resize'];

		//////////////INPUT
		$params["input"]=Verify::filter($params["input"], [
			"file"=>[
				"require"=>true,
				"type"=>'string',
				"min"=>2,
				"max"=>255
			]
		]);
		if(isset($params["error"])) return $params;

		$params["input"]=self::load($params["input"]);
		if(isset($params["input"]["error"])) return $params["input"];

		//CENTERED/////////////////
		if(
			(isset($params["new_size"]) AND isset($params["new_size"]["centered"]) AND $params["new_size"]["centered"]==1) OR
			(isset($params["base_size"]) AND isset($params["base_size"]["centered"]) AND $params["base_size"]["centered"]==1)
		){
			$params["input"]["centered"] = self::centered($params["input"], []);
		}

		//NEW SIZE////////////
		if(isset($params["new_size"])){
			$params["new_size"]=Verify::filter($params["new_size"], [
				"height"=>[
					"require"=>true,
					"type"=>'int',
					"min"=>1,
					"max"=>3000
				],
				"width"=>[
					"require"=>true,
					"type"=>'int',
					"min"=>1,
					"max"=>3000
				],
				"centered"=>[
					"type"=>'int',
					"min"=>0,
					"max"=>1
				],
				"save_tag"=>[
					"type"=>'string',
					"min"=>2,
					"max"=>128
				],
				"save_dir"=>[
					"type"=>'string',
					"min"=>0,
					"max"=>255
				],
				"save_webdir"=>[
					"type"=>'string',
					"min"=>0,
					"max"=>255
				],
			]);
			if(isset($params["new_size"]["error"])) return $params["new_size"];

			$params["new_size"]["resource"] = self::resize_to_center($params["input"], $params["new_size"]);

			if(isset($params["new_size"]["save_tag"])){
				if(!isset($params["new_size"]["save_dir"])) $params["new_size"]["save_dir"]=IMG_DIR.'/';
				$save_name=Files::new_name($params["new_size"]["save_dir"], $params["new_size"]["save_tag"].'.jpg');
				//echo $params["new_size"]["save_dir"].$save_name.'<br />';
				imagejpeg($params["new_size"]["resource"], $params["new_size"]["save_dir"].$save_name);
				$params["new_size"]["save_name"]=$save_name;
			}
		}

		//BASE SIZE////////////
		if(isset($params["base_size"])){
			$params["base_size"]=Verify::filter($params["base_size"], [
				"sizes"=>[
					"type"=>'array_of_string',
					"min"=>2,
					"max"=>120
				],
				"centered"=>[
					"type"=>'int',
					"min"=>0,
					"max"=>1
				],
				"save"=>[
					"type"=>'int',
					"min"=>0,
					"max"=>1
				],
			]);
			if(isset($params["base_size"]["error"])) return $params["base_size"];

			if(!isset($params["base_size"]["sizes"]) OR $params["base_size"]["sizes"][0]!='none'){

				if(!isset($params["base_size"]["sizes"]) OR $params["base_size"]["sizes"][0]=='all') $params["base_size"]=self::$sizes;
				if(!isset($params["base_size"]["save"])) $params["base_size"]["save"]=1;
				if(!isset($params["base_size"]["centered"])) $params["base_size"]["centered"]=1;

				foreach($params["base_size"] as $size_name){
					if(!in_array($size_name, self::$sizes)) continue;

					self::$config["size"]["$size_name"]["width"]=Core::config('img_'.$size_name.'_size_w');
					$params["base_size"][$size_name]["resource"] = self::resize_to_center($params["input"], ["height"=>self::$config["size"]["$size_name"]["height"], "width"=>self::$config["size"]["$size_name"]["width"]]);

					if($params["base_size"]["save"]){
						$save_name=Files::new_name(IMG_DIR.'/', $size_name.'.jpg');
						imagejpeg($params["base_size"][$size_name]["resource"], IMG_DIR.'/'.$save_name);
					}
				}
			}
		}

		return $params;
	}

	private static function resize_to_center($input, $output){
			$img_out=imagecreatetruecolor($output["width"], $output["height"]);
			$input_aspect=$input["width"]/$input["height"]; //отношение ширины к высоте исходника
			$output_aspect=$output["width"]/$output["height"]; //отношение ширины к высоте выходного

			if($input_aspect < $output_aspect){//узкий вариант (фиксированная ширина)
				$scale = $output["width"]/ $input["width"];
				$output_size = array($output["width"], $output["width"] / $input_aspect);
				$input_pos = array(0, ($input["height"] * $scale - $output["height"]) / $scale / 2); //Ищем расстояние по высоте от края картинки до начала картины после обрезки
			}elseif($input_aspect > $output_aspect){//широкий вариант (фиксированная высота)
				$scale = $output["height"] / $input["height"];
				$output_size = array($output["height"] * $input_aspect, $output["height"]);
				$input_pos = array(($input["width"] * $scale - $output["width"]) / $scale / 2, 0); //Ищем расстояние по ширине от края картинки до начала картины после обрезки
			}else{//другое
				$output_size = array($output["width"], $output["height"]);
				$input_pos = array(0,0);
			}

			$output_size[0] = max($output_size[0], 1);
			$output_size[1] = max($output_size[1], 1);

			if(isset($output["centered"]) AND $output["centered"]==1) $resource = $input["centered"];
			else $resource = $input["resource"];
			imagecopyresampled($img_out, $resource, 0, 0, $input_pos[0], $input_pos[1], $output_size[0], $output_size[1], $input["width"], $input["height"]);//Копирование и изменение размера изображения с ресемплированием
			return $img_out;
	}

	private static function centered($input, $params=[]){//Конвертация рисунка в бинарную матрицу Все пиксели отличные от фона получают значение 1

		$params = Verify::filter($params, [
			"color_background"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>16777215
			],
			"color_range"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>255
			],
			"border"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>4000
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["color_background"])) $params["color_background"] = 16581375;//16250871
		if(!isset($params["color_range"])) $params["color_range"] = 700000;
		if(!isset($params["border"])) $params["border"] = 4;

		$matrix = [];
		$coords = ["top"=>false, "bottom"=>false, "left"=>false, "right"=>false, "horizontal"=>[], "vertical"=>[]];

		for($i=0; $i<$input["height"]; $i++){
			$coords["horizontal"][$i] = 0;
			for($k=0; $k<$input["width"]; $k++){

				$rgb = imagecolorat($input["resource"], $k, $i);
				//echo $i.':'.$k.' '.$rgb.'<br />';

				/*list($r, $g, $b) = array_values(imageColorsForIndex($input["resource"], $rgb));
				var_dump($r, $g, $b);echo '<br />';*/

				// если цвет пикселя не равен фоновому заполняем матрицу единицей
				if(
					($rgb - $params["color_range"]) > $params["color_background"] OR 
					($rgb + $params["color_range"]) < $params["color_background"]
				){
					$matrix[$i][$k] = 1;
				}else $matrix[$i][$k] = 0;
				//echo $matrix[$i][$k];
			}
			//echo '<br />';
		}
		for($k=0; $k<$input["width"]; $k++) $coords["vertical"][$k] = 0;

		//horizontal / vertical
		foreach($matrix as $vertical=>$horiz){
			foreach($horiz as $horizontal){
				$coords["horizontal"][$vertical] += $horizontal;
				if($coords["horizontal"][$vertical]>0) break;
			}
		}
		for($w=0; $w<$input["width"]; $w++){
			for($h=0; $h<$input["height"]; $h++){
				$coords["vertical"][$w] += $matrix[$h][$w];
				if($coords["vertical"][$w]>0) break;
			}
		}

		//top/bottom/left/right
		$i=0;
		foreach($coords["horizontal"] as $h){
			if($h>0){
				if($coords["top"] === false) $coords["top"] = $i;
				$coords["bottom"] = $i;
			}
			$i++;
		}
		$i=0;
		foreach($coords["vertical"] as $v){
			if($v>0){
				if($coords["left"] === false) $coords["left"] = $i;
				$coords["right"] = $i;
			}
			$i++;
		}
		$coords["vertical"] = $coords["bottom"] - $coords["top"];
		$coords["horizontal"] = $coords["right"] - $coords["left"];
		
		$img_out=imagecreatetruecolor($coords["horizontal"]+$params["border"]*2, $coords["vertical"]+$params["border"]*2);

		var_dump($coords);exit;
		foreach($coords["vertical"] as $vertical){
			
		}
		
		return $matrix;
	}
	/**
	* Выводит матрицу на экран
	* @param type $binaryMartix
	*/
	function printMatrix($binaryMartix) {
		$return = '';
		for ($i = 0; $i < count($binaryMartix); $i++) {
			$return .= "\n";
			for ($j = 0; $j < count($binaryMartix[0]); $j++) {
			$return .= $binaryMartix[$i][$j]." ";
			}
		}
		file_put_contents($this->dirUpload.$this->curImageName.".txt", $return);
	}
/*public $baseWidth = 150;  // длина эталонного изображения (именно товара внутри картинки)
public $baseHeight = 119; // высота эталонного изображения (именно товара внутри картинки)
public $outImageWidth = 600;  // длина изображения после обработки
public $outImageHeight = 400; // высота изображения после обработки
public $im = null;  // дескриптор картитнки
public $binaryMartix = null; // матричное представление картитнки
public $saveBinaryMartixTofile = false; // сохранять матричное представление картитнки в файл
public $dir = 'image/source/';  // расположения набора картинок
public $saveMartix = false; // сохранять матричное представление картитнок
public $extensions = array("", "gif", "jpeg", "png"); // допустимые картинки
public $curImageWidth = 1;  // ширина обрабатываемого изображения
public $curImageHeight = 1; // высота обрабатываемого изображения
public $imgFunctionProcess = "imagecreatefromjpeg"; // функция для работы с изображением
public $curExt = ""; // расширение картинки
public $curImageName = ""; // расширение картинки
public $dirUpload = 'image/result/'; // папка для выгрузки (должна быть создана)*/


function __construct2($path, $w, $h) {
	$this->outImageWidth = $w;
	$this->outImageHeight = $h;
	$this->curImageName = $path;

	list($this->curImageWidth, $this->curImageHeight, $type) = getimagesize($this->dir.$path); // Получаем размеры и тип изображения (число)
	$ext = $this->extensions[$type];
	if ($ext) {
		$this->imgFunctionProcess = 'imagecreatefrom'.$ext; // Получаем название функции, соответствующую типу, для создания изображения
		$func = $this->imgFunctionProcess;
		$this->curExt = $ext;
		$this->im = $func($this->dir.$path); // Создаём дескриптор для работы с исходным изображением
		if (!$this->im) {
		return false;
		}
	} else {
		echo 'Ошибка: Неизвестный формат изображения!';
		return false;
	}

	$this->binaryMartix = $this->imageToMatrix($this->im, false);

	if ($this->saveBinaryMartixTofile) {
		$this->printMatrix($this->binaryMartix);
	}

	$result = $this->explodeMatrix($this->binaryMartix);
	$width = $result['resultInterval'];
	$cropX = $result['startInterval'];

	$this->binaryMartix = $this->imageToMatrix($this->im, true);
	$result = $this->explodeMatrix($this->binaryMartix);
	$height = $result['resultInterval'];
	$cropY = $result['startInterval'];

	$resultult = "Размеры изображения (".$path.") <br/>width=".$this->curImageWidth."px; <br/>	height=".$this->curImageHeight."px;";
	$resultult .= "<br/>Размеры изделия внутри изображения <br/>width=".$width."px; <br/>	height=".$height."px;";
	$resultult .= "<br/>Коэффициенты сжатия <br/>width=".$this->baseWidth / $width."px; <br/>	height=".$this->baseHeight / $height."px;";
	$resultult .= "<br/>Отрезать картинку с точки <br/>cropX=".$cropX." <br/>	cropY=".$cropY." ";

	//$this->crop("2.png", $cropY, $cropX, $width, $height); // Вызываем функцию
	//$this->reSizeImage($name, $ext, $tmp, 0.3);
	echo $resultult;
	if ($this->baseHeight < $height) {
		$this->resizeImage($this->baseHeight / $height);
	} else {
		$this->resizeImage(1);
	};

	imagedestroy($this->im);
}


function explodeMatrix($binaryMartix) {
	$temp = array();

	// сложение столбцов для выявления интервалов
	for ($i = 0; $i < count($binaryMartix); $i++) {
		$sum = 0;
		for ($j = 0; $j < count($binaryMartix[0]); $j++) {
		$sum += $binaryMartix[$i][$j];
		}
		$temp[] = $sum ? 1 : 0;
	}


	// вычисление интервалов по полученной строке
	$start = false;
	$countPart = 0;
	$arrayInterval = array();
	foreach ($temp as $k => $v) {

		if ($v == 1 && !$start) {
		$arrayInterval[$countPart]['start'] = $k;
		$start = true;
		}

		if ($v == 0 && $start) {
		$arrayInterval[$countPart]['end'] = $k - 1;
		$start = false;
		$countPart++;
		}
	}

	//отсеиваем помехи (мелкие интервалы), Большая картинка, всяко больше 20px. 

	$resultultInterval = 1;
	$startInterval = 1; // начало интервала
	foreach ($arrayInterval as $key => $interval) {
		if (($interval['end'] - $interval['start']) > 20) {
		$resultultInterval = $interval['end'] - $interval['start'];
		$startInterval = $interval['start'];
		}
	}
	return
		array(
		'resultInterval' => $resultultInterval,
		'startInterval' => $startInterval
	);
}




/**
	* Функция для ресайза картинки
	* @paramint $koef коэффициент сжатия изображения
	* @return void
	*/
public function resizeImage($koef) {
	// получение новых размеров  
	$newWidth = $koef * $this->curImageWidth;
	$newHeight = $koef * $this->curImageHeight;
	// ресэмплирование    
	$image_p = imagecreatetruecolor($this->outImageWidth, $this->outImageHeight);
	//делаем фон изображения белым, иначе в png при прозрачных рисунках фон черный
	$color = imagecolorallocate($image_p, 255, 255, 255);
	imagefill($image_p, 0, 0, $color);
	imagecopyresampled(
		$image_p, $this->im, ($this->outImageWidth - $newWidth) / 2, ($this->outImageHeight - $newHeight) / 2, 0, 0, $newWidth, $newHeight, $this->curImageWidth, $this->curImageHeight
	);
	$func = "image".$this->curExt;
	$func($image_p, $this->dirUpload.$this->curImageName);
	imagedestroy($image_p);
}

}// конец тела класса

/**
* Массив имен файлов
*/
$imagesName = array(
'kameya_medium_1005858593.jpg',
'kameya_medium_1005862834.jpg',
'km0210-3_small.jpg',
'sd0201966_small.jpg',
);

/**
* Перебор массива имен файлов и нормализация изображений
*/
foreach ($imagesName  as $image) {
if (file_exists('image/source/'.$image)) {
	// каждую картинку нормальзуем и приведем к разрешению 200x200 пикселей
	$encrypt = new imageWorker($image, 200, 200);
} else {
	continue;
}
}