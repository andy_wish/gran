<?php if (!defined("INBOX")) die('separate call');

class Person {

	public static function create($params){

		$params = Verify::filter($params, self::DATA_DESC);
		if(isset($params["error"])) return $params;

		//////SET
		$into = 'INTO `person` (';
		$values = 'VALUES (';
		foreach($params as $name  => $val) {
			switch($name){
				case 'closed':
					if($val==1){
						$into.= '`person`.`'.$name.'`, ';
						$values.= 'NOW(), ';
					}
					continue 2;

				default:
					$into.= '`person`.`'.$name.'`, ';
					$values.= DB::parse('?s, ', $val);
			}
		}
		if(strlen($into)>15 AND strlen($values)>9){
			$into.= ')';
			$values.= ')';
		}else return ['error'=>'nothing to insert'];

		$q = 'INSERT '.$into.' '.$values;
		DB::query($q);
		$id = DB::insertId();
		if($id){
			User::logging(10, $id);
			return ["id" =>$id];
		}else return ['error'=>'db error'];

		return $result;
	}

	public static function from_file($params){

		//var_dump($params);//exit;

		if(self::$positions === false){
			$position_db = DB::getAll("SELECT `id`, `name` FROM `basis` WHERE `type`='position'");
			self::$positions = [];
			foreach($position_db as $position){
				self::$positions[$position["name"]] = $position["id"];
			}
		}

		$params["name"] = explode(' ', $params["name"]);
		$params["surname"] = $params["name"][0];
		if(isset($params["name"][2])) $params["patronymic"] = $params["name"][2];
		else $params["patronymic"] = '';
		$params["name"] = $params["name"][1];

		$params["finish_desc"] = trim($params["finish_desc"]);
		//if($params["finish_desc"]=='') $params["finish_desc"] = null;
		$params["start_desc"] = trim($params["start_desc"]);
		//if($params["start_desc"]=='') $params["start_desc"] = null;

		//citizen
		switch($params["citizen"]){
			case 'РОССИЯ':
			case 'РОСCИЯ':
			case 'РОCСИЯ':
			
				$params["citizen"] = 34;
				break;
			case 'УКРАИНА':
				$params["citizen"] = 35;
				break;
			case 'КАЗАХСТАН':
				$params["citizen"] = 36;
				break;
			case 'УЗБЕКИСТАН':
				$params["citizen"] = 37;
				break;
			default:
				die('unknown citizen code: '.$params["citizen"]);
		}

		//status
		switch($params["status"]){
			case 'Работа':
			case 'РАБОТА':
			case 'работа':
				$params["status"] = 41;
				break;
			case 'УВОЛЕН':
			case 'Уволен':
			case 'уволен':
				$params["status"] = 42;
				break;
			case 'Отпуск неоплачиваемый по разрешению работодателя':
			case 'Отпуск неоплачиваемый по разрешению работодателя 04.05-15.05';
			case 'Отпуск неоплачиваемый по разрешению работодателя 28.04 -15.05';
				$params["status"] = 646;
				break;
			default:
				//Простой, не зависящий от работодателя и работника  21.04.2020-04.05.2020
				//Простой, отстранение, не зависящий от работодателя и работника
				$pos = stripos($params["status"], 'Простой,');
				if($pos !== false){
					$params["status"] = 43;
				}else {
					die('unknown status code: '.$params["status"]);
				}
				break;
		}

		//place
		switch($params["place"]){
			case 'Участок Анич':
				$params["place"] = 38;
				break;
			case 'участок Дусканья':
				$params["place"] = 39;
				break;
			case 'Усть-Омчуг':
				$params["place"] = 40;
				break;
			case 'Магадан':
				$params["place"] = 1868;
				break;
			case '':
				$params["place"] = 0;
				break;
			default:
				die('unknown place code: '.$params["place"]);
				break;
		}

		//position
		if(array_key_exists($params["position"], self::$positions)){
			$params["position"] = self::$positions[$params["position"]];
			//echo $params["position"];
		}else{
			DB::query("INSERT INTO `basis` (`name`, `type`)
			VALUES (?s, 'position')", $params["position"]);
			$position_id = DB::insertId();
			if(!$position_id) die('failed to get position_id from: '. $params["position"]);
			else{
				self::$positions[$params["position"]] = $position_id;
				$params["position"] = $position_id;
			}
		}

		//dates
		if(strlen($params["birth_date"]) == 10){
			$date = date_create_from_format("d.m.Y", $params["birth_date"]);
			$params["birth_date"] = $date->format('Y-m-d');
		}elseif(strlen($params["birth_date"]) == 5){
			$date_excel = new DateTime('30.12.1899');
			$date_excel->add(new DateInterval('P'.$params["birth_date"].'D'));
			$params["birth_date"] = $date_excel->format('Y-m-d');
		}else	$params["birth_date"] = null;

		if(strlen($params["start_date"]) == 10){
			$date = date_create_from_format("d.m.Y", $params["start_date"]);
			$params["start_date"] = $date->format('Y-m-d');
		}elseif(strlen($params["start_date"]) == 5){
			$date_excel = new DateTime('30.12.1899');
			$date_excel->add(new DateInterval('P'.$params["start_date"].'D'));
			$params["start_date"] = $date_excel->format('Y-m-d');
		}else $params["start_date"] = null;

		if(strlen($params["finish_date"]) == 10){
			$date = date_create_from_format("d.m.Y", $params["finish_date"]);
			$params["finish_date"] = $date->format('Y-m-d');
		}elseif(strlen($params["start_date"]) == 5){
			$date_excel = new DateTime('30.12.1899');
			$date_excel->add(new DateInterval('P'.$params["finish_date"].'D'));
			$params["finish_date"] = $date_excel->format('Y-m-d');
		}else $params["finish_date"] = null;



		$person_db = DB::getRow('SELECT * FROM `person` WHERE `name`=?s AND `surname`=?s AND `birth_date`=?s LIMIT 1', $params["name"], $params["surname"], $params["birth_date"]);

		if($person_db) {

			if(
				$params["position"] != $person_db["position"] OR
				$params["place"] != $person_db["place"] OR
				$params["birth_date"] != $person_db["birth_date"] OR
				$params["citizen"] != $person_db["citizen"] OR
				$params["start_date"] != $person_db["start_date"] OR
				$params["start_desc"] != $person_db["start_desc"] OR
				$params["finish_date"] != $person_db["finish_date"] OR
				$params["finish_desc"] != $person_db["finish_desc"] OR
				$params["status"] != $person_db["status"]
			){

/*var_dump($params);//exit;
var_dump($person_db);
				
var_dump($params["position"] != $person_db["position"]);
var_dump($params["place"] != $person_db["place"]);
var_dump($params["birth_date"] != $person_db["birth_date"]);
var_dump($params["citizen"] != $person_db["citizen"]);
var_dump($params["start_date"] != $person_db["start_date"]);
var_dump($params["start_desc"] != $person_db["start_desc"]);
var_dump($params["finish_date"] != $person_db["finish_date"]);
var_dump($params["finish_desc"] != $person_db["finish_desc"]);
var_dump($params["status"] != $person_db["status"]);
exit;*/

				$q='UPDATE `person` SET
					`position`=?i, `place`=?i, `birth_date`=?s, `citizen`=?i, `start_date`=?s, `start_desc`=?s, `finish_date`=?s, `finish_desc`=?s, `status`=?i
				WHERE 
					`id`=?i
				LIMIT 1';
				if(DB::query($q, $params["position"], $params["place"], $params["birth_date"], $params["citizen"], $params["start_date"], $params["start_desc"], $params["finish_date"], $params["finish_desc"], $params["status"], $person_db["id"])){
					User::logging(11, $person_db["id"]);
					return ["id" =>$person_db["id"], "desc" => 'updated'];
				}else return ['error'=>'db update error'];
			}else return ["id" =>$person_db["id"], "desc" => 'exist similar one'];

		}else{
			$q='INSERT INTO `person` (`name`, `surname`, `patronymic`, `position`, `place`, `birth_date`, `citizen`, `start_date`, `start_desc`, `finish_date`, `finish_desc`, `status`)
				VALUES (?s, ?s, ?s, ?i, ?i, ?s, ?i, ?s, ?s, ?s, ?s, ?i)';
			DB::query($q, $params["name"], $params["surname"], $params["patronymic"], $params["position"], $params["place"], $params["birth_date"], $params["citizen"], $params["start_date"], $params["start_desc"], $params["finish_date"], $params["finish_desc"], $params["status"]);
			$id = DB::insertId();
			if($id){
				User::logging(10, $id);
				return ["id" =>$id, "desc" => 'inserted new'];
			}else return ['error'=>'db error'];
		}
	}

	public static function read($params){

		$data_desc = self::DATA_DESC;
		$data_desc["search_str"] = [
			"type"=>'string',
			"min"=>2,
			"max"=>255
		];
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["closed"])) $params["closed"] = 0;

		///RESPONSE
		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by = '`person`.`id`';
					break;
				case 'name':
					$order_by = '`person`.`name`';
					break;
				case 'surname':
					$order_by = '`person`.`surname`';
					break;
				case 'patronymic':
					$order_by = '`person`.`patronymic`';
					break;
				case 'position':
					$order_by = '`person`.`position`';
					break;
				case 'place':
					$order_by = '`person`.`place`';
					break;
				case 'address':
					$order_by = '`person`.`address`';
					break;
				case 'phone':
					$order_by = '`person`.`phone`';
					break;
				case 'birth_date':
					$order_by = '`person`.`birth_date`';
					break;
				case 'citizen':
					$order_by = '`person`.`citizen`';
					break;
				case 'start_date':
					$order_by = '`person`.`start_date`';
					break;
				case 'finish_date':
					$order_by = '`person`.`finish_date`';
					break;
				case 'status':
					$order_by = '`person`.`status`';
					break;
				case 'created':
					$order_by = '`person`.`created`';
					break;
				case 'closed':
					$order_by = '`person`.`closed`';
					break;
				default:
					$order_by = '`person`.`id`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='desc') $order_dir = 'DESC';
			else $order_dir = 'ASC';
			$order_by = 'ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by = 'ORDER BY `person`.`start_date`, `person`.`surname`';
		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit = 'LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit = 'LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit = 'LIMIT 40';
		unset($params["response"]);

		/////WHERE
		$where = 'WHERE ';
		foreach($params as $name=>$value){
			switch($name){
/*				case 'meta':
				case 'meta_get_value':
					break;*/
				case 'id':
					$where.= DB::parse('`person`.`'.$name.'`=?i AND ', $value);
					break;
				case 'search_str':
					$where.= DB::parse("(
						`person`.`name` LIKE(?s) OR
						`person`.`surname` LIKE(?s) OR
						`person`.`patronymic` LIKE(?s) OR 
						`person`.`address` LIKE(?s)) AND ", '%'.$value.'%', '%'.$value.'%', '%'.$value.'%', '%'.$value.'%');
					break;
				case 'closed':
					if(!$value){
						$where.= '`person`.`closed` IS NULL AND ';
					}elseif($value == 1){
						$where.='`person`.`closed` IS NOT NULL AND ';
					}
					break;
				default:
					$where.=DB::parse('`person`.`'.$name.'`=?s AND ', $value);
			}
		}
		if(strlen($where)>6) $where = rtrim($where, 'AND ');
		else $where = '';

		//$result = [];

		$q = 'SELECT COUNT(`id`) FROM `person` '.$where;
//die($q);
		$result["total_db"] = (int)DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"] = 0;
			$result["list"] = [];
			return $result;
		}

		/////SELECT
		$select = '
			SELECT 
				`person`.*,
				DATE_FORMAT(`person`.`birth_date`, "'.Core::config("mysql_date_article_nice").'") AS `birth_date_nice`,
				DATE_FORMAT(`person`.`start_date`, "'.Core::config("mysql_date_article_nice").'") AS `start_date_nice`,
				DATE_FORMAT(`person`.`finish_date`, "'.Core::config("mysql_date_article_nice").'") AS `finish_date_nice`,
				DATE_FORMAT(`person`.`created`, "'.Core::config("mysql_date_article_nice").'") AS `created_nice`,
				(SELECT `basis`.`name` FROM `basis` WHERE `basis`.`id`=`person`.`position` LIMIT 1) AS `position_name`,
				(SELECT `basis`.`name` FROM `basis` WHERE `basis`.`id`=`person`.`place` LIMIT 1) AS `place_name`,
				(SELECT `basis`.`name` FROM `basis` WHERE `basis`.`id`=`person`.`citizen` LIMIT 1) AS `citizen_name`,
				(SELECT `basis`.`name` FROM `basis` WHERE `basis`.`id`=`person`.`status` LIMIT 1) AS `status_name`
				
				';

		$q = $select.'
			FROM `person`
			'.$where.'
			'.$order_by.'
			'.$limit;
//echo($q);die();
		$list = DB::getAll($q);


		if(!$list){
			$result["total"] = 0;
			return $result;
		}

		$id_current = false;
		$total = 0;
		foreach($list as $row){
			foreach($row as $name=>$value){
//echo $name.' => '.$value.'<br />';

				if($value=='') {
					$result["list"]["$total"]["$name"] = '';
					continue;
				}
				$result["list"]["$total"]["$name"] = $value;
			}
//echo '<hr />';
			$total++;
		}
		$result["total"] = $total;

		//echo '<pre>';var_dump($result["list"][0]);exit;
		//echo '<pre>';var_dump($result);die();

		return $result;
	}

	public static function update($params){

		$data_desc = self::DATA_DESC;
		$data_desc["id"][] = ["require"=>true];
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `person` WHERE `id`=?i LIMIT 1', $params["id"])) return ['error'=>'person ['.$params["id"].'] not found'];

		//////SET
		$set = 'SET ';
		foreach($params as $name => $val) {
			switch($name){
				case 'id':
					continue 2;
				case 'closed':
					if($val==0) $set.=DB::parse('`person`.`'.$name.'`=NULL, ');
					else $set.=DB::parse('`person`.`'.$name.'`=NOW(), ');
					continue 2;
				default:
					$set.=DB::parse('`person`.`'.$name.'`=?s, ', $val);
			}
		}
		if(strlen($set)>5) $set=rtrim($set, ', ');
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `person` '.$set.', `user_id`=?i, `ip`=?s WHERE `person`.`id`=?i LIMIT 1', User::id(), User::ip(), $params["id"])){
			User::logging(11, $params["id"]);
			return array("id"=>$params["id"]);
		}else return ['error'=>'db error'];
	}

	public static function delete($params){
		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"wipe"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["wipe"]) OR $params["wipe"]==0){
			DB::query('UPDATE `person` SET `closed`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='';
		}else{
			DB::query('DELETE FROM `person` WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='wipe';
		}
		if(DB::affectedRows()) {
			User::logging(12, $params["id"], $log_comment);
			return ["id"=>$params["id"]];
		}else return ['error'=>'db error / not found'];
	}


	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}


	protected static $positions = false;

	protected const DATA_DESC=[//
			"id"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>18446744073709551615
			],
			"name"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"surname"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"patronymic"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"datetime"=>[
				"type"=>'string',//2019-10-02 15:20:00
				"min"=>19,
				"max"=>19
			],
			"phone"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"address"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"img"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"created"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"place"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>4294967295
			],
			"position"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>4294967295
			],
			"description"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"radio"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>7
			],
			"closed"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>2
			],
			"response"=>[
				"type"=>'helper'
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>255
			]
		];

}