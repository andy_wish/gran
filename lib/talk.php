<?php if (!defined("INBOX")) die('separate call');

class Talk {

	public static function create($params){

		$data_desc=self::DATA_DESC;
		unset($data_desc["id"]);
		$data_desc["datetime"]["require"]=true;
		$data_desc["phone_to"]["require"]=true;
		$data_desc["code"]["require"]=true;
		$data_desc["minute"]["require"]=true;
		$data_desc["direction"]["require"]=true;
		$data_desc["cost"]["require"]=true;
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		$result = ["id" => false, "exist" => 0];

		$result["id"] = DB::getOne("SELECT `id` FROM `talk` WHERE `datetime`=?s AND `code`=?s LIMIT 1", $params["datetime"], $params["code"]);
		if($result["id"]){
			$result["exist"] = 1;
			return $result;
		}

		$q = "INSERT INTO `talk`(`datetime`, `phone_to`, `code`, `minute`, `direction`, `cost`) VALUES(?s, ?s, ?s, ?i, ?s, ?i)";
		DB::query($q, $params["datetime"], $params["phone_to"], $params["code"], $params["minute"], $params["direction"], $params["cost"]);
		$result["id"] = DB::insertId();
		if($result["id"]){
			//User::logging(10, $id);
			return $result;
		}else return ['error'=>'db error'];

	}

	public static function code_create($params){

		$data_desc = [
			"code" => [
				"type" => 'string',
				"min" => 10,
				"max" => 10,
				"require" => true
			],
			"name" => [
				"type" => 'string',
				"min" => 1,
				"max" => 255,
				"require" => true
			],
		];
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		$code = DB::getRow("SELECT `id`, `name`, `content` AS `code` FROM `basis` WHERE `type`='phonecode' AND `content`=?s LIMIT 1", $params["code"]);
		if($code){
			if ($params["name"] == $code["name"]){
				return ["id" => $code["id"], "exist" => 1, "action" => 0];
			}else{
				DB::query("UPDATE `basis` SET `name`=?s WHERE `id`=?i LIMIT 1", $params["name"], $code["id"]);
				return ["id" => $code["id"], "exist" => 1, "action" => 1];
			}
		}
		
		$q = "INSERT INTO `basis`(`name`, `content`, `type`) VALUES(?s, ?s, 'phonecode')";
		DB::query($q, $params["name"], $params["code"]);
		$result["id"] = DB::insertId();
		if($result["id"]){
			//User::logging(10, $id);
			$result["exist"] = 0;
			$result["action"] = 2;
			return $result;
		}else return ['error'=>'db error'];

	}

	public static function read($params){//

		$data_desc=self::DATA_DESC;
		$data_desc["lang"]["max"]=3;
		$data_desc["search_str"]=[
			"type"=>'string',
			"min"=>2,
			"max"=>255
		];
		$data_desc["types"]=[////////////
			"type"=>'array_of_string',
			"min"=>1,
			"max"=>255
		];
		$params=Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["lang"])) $params["lang"]=Lang::current_lang();
		if(!isset($params["closed"])) $params["closed"]=0;
		if(!isset($params["meta"])) $params["meta"]=0;

		///RESPONSE
		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`talk`.`id`';
					break;
				case 'id_parent':
					$order_by='`talk`.`id_parent`';
					break;
				case 'name':
					$order_by='`talk`.`name`';
					break;
				case 'name_translit':
					$order_by='`talk`.`name_translit`';
					break;
				case 'type':
					$order_by='`talk`.`type`';
					break;
				case 'content':
					$order_by='`talk`.`content`';
					break;
				case 'title':
					$order_by='`talk`.`title`';
					break;
				case 'description':
					$order_by='`talk`.`description`';
					break;
				case 'keywords':
					$order_by='`talk`.`keywords`';
					break;
				case 'lang':
					$order_by='`talk`.`lang`';
					break;
				case 'order_num':
					$order_by='`talk`.`order_num`';
					break;
				case 'datetime':
					$order_by='`talk`.`datetime`';
					break;
				case 'img':
					$order_by='`talk`.`img`';
					break;
				case 'target':
					$order_by='`talk`.`target`';
					break;
				case 'popular':
					$order_by='`talk`.`popular`';
					break;
				case 'mime_type':
					$order_by='`talk`.`mime_type`';
					break;
				case 'created':
					$order_by='`talk`.`created`';
					break;
				case 'closed':
					$order_by='`talk`.`closed`';
					break;
				default:
					$order_by='`talk`.`order_num`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='desc') $order_dir='DESC';
			else $order_dir='ASC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `talk`.`order_num`, `talk`.`id`';
		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';
		unset($params["response"]);

		/////WHERE
		$where='WHERE ';
		foreach($params as $name=>$value){
			switch($name){
				case 'meta':
				case 'meta_get_value':
					break;

				case 'lang':
					if($name!='all') $where.=DB::parse('`talk`.`'.$name.'`=?s AND ', $value);
					break;
				case 'id':
				case 'id_parent':
					$where.=DB::parse('`talk`.`'.$name.'`=?i AND ', $value);
					break;
				case 'search_str':
					$where.=DB::parse("(`talk`.`name` LIKE(?s) OR `talk`.`content` LIKE(?s)) AND ", '%'.$value.'%', '%'.$value.'%');
					break;
				case 'closed':
					if(!$value){
						$where.='`talk`.`closed` IS NULL AND ';
					}else{
						$where.='`talk`.`closed` IS NOT NULL AND ';
					}
					break;
				default:
					$where.=DB::parse('`talk`.`'.$name.'`=?s AND ', $value);
			}
		}
		if(strlen($where)>5) $where=rtrim($where, 'AND ');
		else $where='';

		//$result=[];

		$q='SELECT COUNT(`id`) FROM `talk` '.$where;
		//die($q);
		$result["total_db"]=(int)DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"]=0;
			$result["list"]=[];
			return $result;
		}

		/////SELECT
		$select='
			SELECT 
				`talk`.*, DATE_FORMAT(`talk`.`created`, "'.Core::config("mysql_date_comment_nice").'") AS `created_nice`,
				`parent`.`name` AS `name_parent`, `parent`.`name_translit` AS `name_translit_parent`, 
				(SELECT `user`.`name` FROM `user` WHERE `user`.`id`=`talk`.`user_id` LIMIT 1) AS `user_name`,
				(SELECT COUNT(`child`.`id`) FROM `talk` AS `child` WHERE `child`.`id_parent`=`talk`.`id` GROUP BY `child`.`id_parent`) AS `child_num`';
		if(isset($params["meta_get_value"])) $select.=DB::parse(', (SELECT `talk_meta`.`value` FROM `talk_meta` WHERE `talk_meta`.`talk_id`=`talk`.`id` AND `talk_meta`.`name`=?s LIMIT 1) AS `meta_get_value`', $params["meta_get_value"]);

		$q=$select.'
			FROM `talk`
			LEFT JOIN `talk` AS `parent` ON `parent`.`id`=`talk`.`id_parent`
			'.$where.'
			'.$order_by.'
			'.$limit;
//echo($q);echo '<br />';
		$list=DB::getAll($q);


		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$id_current=false;
		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
//echo $name.' => '.$value.'<br />';
				if($name=='content'){
					$result["list"]["$total"]["$name"]=htmlspecialchars_decode($value);
					continue;
				}

				if($name=='meta_get_value'){
					$name=$params["meta_get_value"];
					$result["list"]["$total"]["meta"]["$name"]=unserialize($value);
					continue;
				}

				if($value=='') {
					$result["list"]["$total"]["$name"]='';
					continue;
				}
				$result["list"]["$total"]["$name"]=$value;
			}
//echo '<hr />';
			$total++;
		}
		$result["total"]=$total;

		if(isset($params["meta"]) AND $params["meta"]!==0){//медленный запрос, тестируем

			foreach($result["list"] as &$talk){
				$meta_params["talk_id"]=$talk["id"];
				if($params["meta"]=='autoload') $meta_params["autoload"]=1;
				$meta=self::read_meta($meta_params);
				if(isset($meta["error"])) return $meta;

				if(!isset($talk["meta"])) $talk["meta"]=$meta;
				else $talk["meta"]=array_merge($talk["meta"], $meta);
				
				$meta=[];
				foreach($talk["meta"]["list"] as $list){
					$meta[$list["name"]] = $list["value"];
				}
				$talk["meta"] = $meta;
			}
			unset($talk);
		}

		//echo '<pre>';var_dump($result["list"][0]);exit;
		//echo '<pre>';var_dump($result);die();

		return $result;
	}


	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}


	protected const DATA_DESC=[
		"id"=>[
			"type"=>'int',
			"min"=>1,
			"max"=>18446744073709551615
		],
		"datetime"=>[//Время звонка//2019-10-02 15:20:00
			"type"=>'string',
			"min"=>19,
			"max"=>19
		],
		"phone_to"=>[//Телефон вызываемого
			"type"=>'string',
			"min"=>8,
			"max"=>16
		],
		"code"=>[//Код (Телефон звонившего)
			"type"=>'string',
			"min"=>10,
			"max"=>10
		],
		"minute"=>[//Количество минут
			"type"=>'int',
			"min"=>0,
			"max"=>4294967296
		],
		"direction"=>[//Название направления
			"type"=>'string',
			"min"=>1,
			"max"=>255
		],
		"cost"=>[//Стоимость звонка, руб
			"type"=>'int',
			"min"=>0,
			"max"=>4294967296
		],
	];

}