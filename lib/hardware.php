<?php if (!defined("INBOX")) die('separate call');

class Hardware {

	public static function create($params){

		$data_desc = self::DATA_DESC;
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		//////SET
		$into = 'INTO `hardware` (';
		$values = 'VALUES (';
		foreach($params as $name  => $val) {
			switch($name){
				case 'closed':
					if($val==1){
						$into.= '`hardware`.`'.$name.'`, ';
						$values.= 'NOW(), ';
					}
					continue 2;

				default:
					$into.= '`hardware`.`'.$name.'`, ';
					$values.= DB::parse('?s, ', $val);
			}
		}
		if(strlen($into)>15 AND strlen($values)>9){
			$into.= ')';
			$values.= ')';
		}else return ['error'=>'nothing to insert'];

		$q = 'INSERT '.$into.' '.$values;
		DB::query($q);
		$id = DB::insertId();
		if($id){
			User::logging(10, $id);
			return ["id" =>$id];
		}else return ['error'=>'db error'];

		return $result;
	}

	public static function read($params){

		$data_desc = self::DATA_DESC;
		$data_desc["search_str"] = [
			"type"=>'string',
			"min"=>2,
			"max"=>255
		];
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["closed"])) $params["closed"] = 0;

		///RESPONSE
		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by = '`hardware`.`id`';
					break;
				case 'name':
					$order_by = '`hardware`.`name`';
					break;
				case 'type':
					$order_by = '`hardware`.`type`';
					break;
				case 'place':
					$order_by = '`hardware`.`place`';
					break;
				case 'ip':
					$order_by = '`hardware`.`ip`';
					break;
				case 'version':
					$order_by = '`hardware`.`version`';
					break;
				case 'created':
					$order_by = '`hardware`.`created`';
					break;
				case 'closed':
					$order_by = '`hardware`.`closed`';
					break;
				default:
					$order_by = '`hardware`.`name`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='desc') $order_dir = 'DESC';
			else $order_dir = 'ASC';
			$order_by = 'ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by = 'ORDER BY `hardware`.`name`';
		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit = 'LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit = 'LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit = 'LIMIT 200';
		unset($params["response"]);

		/////WHERE
		$where = 'WHERE ';
		foreach($params as $name=>$value){
			switch($name){
				case 'id':
					$where.= DB::parse('`hardware`.`'.$name.'`=?i AND ', $value);
					break;
				case 'search_str':
					$where.= DB::parse("(
						`hardware`.`name` LIKE(?s) OR
						`hardware`.`ip` LIKE(?s)) AND ", '%'.$value.'%', '%'.$value.'%', '%'.$value.'%', '%'.$value.'%');
					break;
				case 'closed':
					if(!$value){
						$where.= '`hardware`.`closed` IS NULL AND ';
					}elseif($value == 1){
						$where.='`hardware`.`closed` IS NOT NULL AND ';
					}
					break;
				default:
					$where.=DB::parse('`hardware`.`'.$name.'`=?s AND ', $value);
			}
		}
		if(strlen($where)>6) $where = rtrim($where, 'AND ');
		else $where = '';

		//$result = [];

		$q = 'SELECT COUNT(`id`) FROM `hardware` '.$where;
//die($q);
		$result["total_db"] = (int)DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"] = 0;
			$result["list"] = [];
			return $result;
		}

		/////SELECT
		$select = '
			SELECT 
				`hardware`.*,
				DATE_FORMAT(`hardware`.`created`, "'.Core::config("mysql_date_article_nice").'") AS `created_nice`,
				(SELECT `model`.`name` FROM `model` WHERE `model`.`id`=`hardware`.`model_id` LIMIT 1) AS `model_name`
				';

		$q = $select.'
			FROM `hardware`
			'.$where.'
			'.$order_by.'
			'.$limit;
//echo($q);die();
		$list = DB::getAll($q);


		if(!$list){
			$result["total"] = 0;
			return $result;
		}

		$id_current = false;
		$total = 0;
		foreach($list as $row){
			foreach($row as $name=>$value){
//echo $name.' => '.$value.'<br />';

				if($value=='') {
					$result["list"]["$total"]["$name"] = '';
					continue;
				}
				$result["list"]["$total"]["$name"] = $value;
			}
//echo '<hr />';
			$total++;
		}
		$result["total"] = $total;

		//echo '<pre>';var_dump($result["list"][0]);exit;
		//echo '<pre>';var_dump($result);die();

		return $result;
	}

	public static function update($params){

		$data_desc = self::DATA_DESC;
		$data_desc["id"][] = ["require"=>true];
		$params = Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `hardware` WHERE `id`=?i LIMIT 1', $params["id"])) return ['error'=>'hardware ['.$params["id"].'] not found'];

		//////SET
		$set = 'SET ';
		foreach($params as $name => $val) {
			switch($name){
				case 'id':
					continue 2;
				case 'closed':
					if($val==0) $set.=DB::parse('`hardware`.`'.$name.'`=NULL, ');
					else $set.=DB::parse('`hardware`.`'.$name.'`=NOW(), ');
					continue 2;
				default:
					$set.=DB::parse('`hardware`.`'.$name.'`=?s, ', $val);
			}
		}
		if(strlen($set)>5) $set=rtrim($set, ', ');
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `hardware` '.$set.' WHERE `hardware`.`id`=?i LIMIT 1', $params["id"])){
			User::logging(11, $params["id"]);
			return ["id"=>$params["id"]];
		}else return ['error'=>'db error'];
	}

	public static function delete($params){
		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"wipe"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["wipe"]) OR $params["wipe"]==0){
			DB::query('UPDATE `hardware` SET `closed`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='';
		}else{
			DB::query('DELETE FROM `hardware` WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='wipe';
		}
		if(DB::affectedRows()) {
			User::logging(12, $params["id"], $log_comment);
			return ["id"=>$params["id"]];
		}else return ['error'=>'db error / not found'];
	}

	public static function get_ip_by_id($id){
		return DB::getOne('SELECT `ip` FROM `hardware` WHERE `id`=?i LIMIT 1', $id);
	}


	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	protected const DATA_DESC=[//?
			"id"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>18446744073709551615
			],
			"type"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>65365
			],
			"created"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"model_id"=>[
				"type" => 'int',
				"min" => 1
			],
			"description" => [
				"type" => 'string',
				"min" => 0,
				"max" => 255
			],
			"version" => [
				"type" => 'string',
				"min" => 0,
				"max" => 65365
			],
			"reboot_priority"=>[
				"type" => 'int',
				"min" => 0,
				"max" => 255
			],
			"closed"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>2
			],
			"response"=>[
				"type"=>'helper'
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>255
			]
		];

}